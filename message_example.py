from no_press_pinnacle.messages import Message, MsgOrder, Order, SupportHold, daide_from_dict, dict_from_daide
from no_press_pinnacle.utils import ArmyUnit, FleetUnit, Location
from diplomacy import Game

from no_press_pinnacle.utils.utils import short2long, long2short


SHADE_ORDERS = ["A LON H", "F IRI - MAO", "A IRI - MAO VIA", "A WAL S F LON", 
                "A WAL S F MAO - IRI", "F NWG C A NWY - EDI",
                "A IRO R MAO", "A IRO D", "A LON B", "F LIV B"]


order_objs = []

# Parse orders in SHADE syntax to turn into Order objects
for shade_order in SHADE_ORDERS:
    # NOTE: There is much information lacking in the shade order syntax that is otherwise present in the DAIDE order syntax, it will need to be supplemented
    # NOTE: EX: The path taken by a unit in a Move by Convoy order is calculated within the SHADE engine
    #       orders for move by convoy are executed one space at a time 
    
    # Parse into Order object
    if 'VIA' in shade_order:
        path = ['IRI', 'MAO']
        order = Order.from_shade_str(shade_order, power='ENG', path=path)
    else:
        # For orders that include secondary units (ie a SupportHold such as  A WAL S F LON)
        # You are able to include the power that the secondary unit is affiliated with.  It will otherwise be assgined to a null power "000".
        order = Order.from_shade_str(shade_order, power='ENG', other_power='RUS')
    
    order_objs.append(order)

# Parse DAIDE syntax strings to turn into Message objects
# Also, this showcases turning an Order object into a DAIDE syntax string
message_objs = [Message.from_daide_str("PRP(XDO({}))".format(str(order))) for order in order_objs]

# Messages/Orders can also be constructed as normal objects
# unit object - similarly to Messages and Orders, can be turned into either SHADE or DAIDE syntax strings
unit = ArmyUnit('ENG', Location('PIC'))   # Army unit owned by England at Picardy
other_unit = FleetUnit('GER', Location('BEL')) # Fleet unit owned by Germany at Belgium

# Construct SupportHold message and then feed it into a MsgOrder (XDO) object
support_order = SupportHold(unit, other_unit)
support_msg = MsgOrder(support_order)

# Messages, Orders and Units can all handle comparison
print("Unit comparison: {}".format(unit == ArmyUnit.from_shade_str('A PIC', power='ENG')))
print("Order comparison: {}".format(support_order == Order.from_daide_str("(ENG AMY PIC) SUP (GER FLT BEL)")))
print("Message comparison: {}".format(support_msg == Message.from_daide_str("PRP(XDO((ENG AMY PIC) SUP (GER FLT BEL)))")))

message_objs.append(support_msg)
order_objs.append(support_order)

print("--------------- MESSAGES IN DAIDE ---------------")

# Output to DAIDE
for message in message_objs:
    print(message.compose())    # Compose adds PRP() to wrap around the message

print("--------------- ORDERS IN SHADE ---------------")

# Output orders to SHADE
for order in order_objs:
    print(order.to_shade_str())

print("--------------- ORDERS IN DAIDE ---------------")

# Output orders to DAIDE
for order in order_objs:
    print(str(order))

print("--------------- SHADE DICTIONARY TO DAIDE ---------------")

# From dictionary:
# nego_order_dict = {
#     'AUSTRIA': ['A BUD - SER', 'A VIE - TRI', 'F TRI - ALB'],
#     'ENGLAND': ['F EDI - NTH', 'F LON - ENG', 'A LVP - YOR']
# }

nego_order_dict = {
    'FRANCE': ['A PAR - BUR', 'F BRE - ENG', 'A MAR - SPA'], 
    'RUSSIA': ['A MOS S F SEV', 'A WAR - SIL', 'F SEV H', 'F STP/SC H']
} 

game = Game()

daide_dict_str, statement = daide_from_dict('FRANCE', nego_order_dict, game.get_units())

print(daide_dict_str)

print("--------------- DAIDE TO SHADE DICTIONARY ---------------")

# To dictionary:

print(dict_from_daide(daide_dict_str, statement))