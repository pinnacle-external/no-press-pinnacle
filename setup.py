import os

from setuptools import setup, find_packages

PACKAGE_VERSION = "0.1.2"

setup(name='no_press_pinnacle',
      version=PACKAGE_VERSION,
      description='NO-PRESS diplomacy module for the Pinnacle agent',
      long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
      long_description_content_type='text/markdown',
      url='https://gitlab.com/pinnacle-external/no-press-pinnacle',
      author='Jacob Le',
      author_email='jale@parc.com',
      packages=find_packages(),
      keywords='diplomacy diplomacy-game game negotiation',
      python_requires='>=3.5',
      install_requires=[
            'diplomacy',
            'numpy'
      ],
      include_package_data=True)