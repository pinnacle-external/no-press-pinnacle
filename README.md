# NO-PRESS Pinnacle

NO-PRESS diplomacy module for the Pinnacle agent.

## Getting started
This module has been tested on Python v3.7.13 using Ubuntu 18.04.  Some packages (such as tensorflow) require python version 3.7 or below.

## Installation
This module can be installed as a Python package via:  
```
pip install git+https://gitlab.com/pinnacle-external/no-press-pinnacle.git
```

Note that as of 4/13/2022, this module is in development and the packages required may change.  

### DIPNET

Additionally, this module depends on the SHADE/research repository located here: https://github.com/SHADE-AI/research

Once cloned, the aforementioned repository must be added to the pythonpath.

The dipnet agent requires singularity to run.  Do so by using the `install_singularity.sh` script.

The dipnet agent will need the environment variable `WORKING_DIR` to be set to the absolute path to the current working directory prior to running.  This is required to download the DIPNET agent's models and the container that will run the agent.

## Usage

### Interfaces module
The main interfaces to the NO-PRESS agent systems.  Instantiating the `DipNetNoPressInterface` should automatically download the DipNet model and container if the `WORKING_DIR` environment variable is set.  Used to query the agent - see example below:
```
from no_press_pinnacle.interfaces import DipNetNoPressInterface

interface = DipNetNoPressInterface()
orders = yield interface.get_beam_orders('RUS')
```

### Utils module
Queries made to the MILA game engine (regarding base game rules and game state) can be done through the utils submodule.
```
from no_press_pinnacle.interfaces import DipNetNoPressInterface
from no_press_pinnacle.utils.mila_engine_utils import get_possible_orders_for_power
from diplomacy import Game

interface = DipNetNoPressInterface()
game = interface.get_game()

orders = get_possible_orders_for_power(game, 'RUS')
```