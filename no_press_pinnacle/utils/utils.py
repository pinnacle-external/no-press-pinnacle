import logging
import sys
from enum import Enum

from no_press_pinnacle.utils.constants import *

def short2long(power: str):
    s2l = {
        ENG : ENGLAND,
        FRA : FRANCE,
        RUS : RUSSIA,
        TUR : TURKEY,
        ITA : ITALY,
        GER : GERMANY,
        AUS : AUSTRIA
    }
    try:
        return s2l[power]
    except KeyError:
        raise KeyError("Invalid power, cannot be lengthened: {}".format(power))

def long2short(power: str):
    l2s =  {
        ENGLAND : ENG,
        FRANCE : FRA,
        RUSSIA : RUS,
        TURKEY : TUR,
        ITALY : ITA,
        GERMANY : GER,
        AUSTRIA : AUS
    }
    try:
        return l2s[power]
    except KeyError:
        raise KeyError("Invalid power, cannot be shortened: {}".format(power))

def create_logger(logger_name: str, log_output: str = None, log_level=logging.INFO):
    """
    Creates a logger object
    :param logger_name: name of the logger object
    :return: Logger object
    """

    if log_output is not None:
        logging.basicConfig(filename=log_output, filemode='a', format='%(name)s - %(asctime)s - %(levelname)s - %(message)s')
    else:
        logging.basicConfig(format='%(name)s - %(asctime)s - %(levelname)s - %(message)s')

    logger = logging.getLogger(logger_name)

    # log_output is set to output to a file, output to stdout as well
    if log_output is not None:
        logger.addHandler(logging.StreamHandler(sys.stdout))

    logger.setLevel(log_level)
    return logger