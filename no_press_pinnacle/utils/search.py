import heapq

from no_press_pinnacle.utils.graph import Node, Graph

def search(graph: Graph, start: Node, end: Node):    # TODO: update to take into consideration unit type

    came_from = {}
    cost_so_far = {}

    frontier = []
    heapq.heappush(frontier, (0, start))

    came_from[start] = start
    cost_so_far[start] = 0

    while len(frontier) > 0:
        current = heapq.heappop(frontier)[1]

        if current == end:  # Early exit
            break

        for n in current.neighbors:
            new_cost = cost_so_far[current] + 1 # Every move has turn cost of 1
            if n not in cost_so_far or new_cost < cost_so_far[n]:
                cost_so_far[n] = new_cost
                
                heapq.heappush(frontier, (new_cost, n))
                came_from[n] = current

    # Exit, now unravel the path
    c = end
    path = []
    if end not in came_from:    # Not in the path
        return []

    while c != start:
        path.append(c)
        c = came_from[c]

    path.reverse()
    return path
