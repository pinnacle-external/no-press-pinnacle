from __future__ import annotations

from dataclasses import dataclass
from typing import List, Set, Dict, Any

from diplomacy import Game
from diplomacy.utils.splitter import OrderSplitter
# from diplomacy_research.models import state_space

from no_press_pinnacle.utils.utils import long2short

SHADE_2_DAIDE_COAST = {
    'NC': 'NCS',
    'SC': 'SCS',
    'WC': 'WCS',
    'EC': 'ECS'
}

DAIDE_2_SHADE_COAST = {
    'NCS': 'NC',
    'SCS': 'SC',
    'WCS': 'WC',
    'ECS': 'EC'
}

def get_unit_loc(unit: str) -> str:
    """
    Given a unit, get the location of the unit
    :param unit: string representation of a unit (ie 'A MOS')
    :return: string representation of a board location
    """
    # [A, MOS] split by space, return the location
    return unit[2:]

def get_unit_owner(unit: str, game:Game) -> str:
    """Given a unit, get the power that owns the unit

    Args:
        unit (str): SHADE notation for the unit in question
        game (Game): game object that contains the state

    Raises:
        ValueError: Raised if the unit name is not found to be owned by any power.

    Returns:
        str: power that owns the unit
    """

    for power_name, unit_list in game.get_units().items():
        units = [u.replace('*', '') for u in unit_list] # Handle equivalency checks on units marked as displaced
        if unit in units:
            return power_name
    
    raise ValueError("Unit {} is not owned by any power!".format(unit))

def get_all_fleets(game: Game) -> List[str]:
    """Returns all fleets that are on the water

    Args:
        game (Game): _description_

    Returns:
        List[str]: _description_
    """
    fleets = []
    for power in game.powers.values():
        for unit in power.units:
            if unit[0] == 'F' and game.map.area_type(unit[2:]) in ['WATER', 'PORT']:
                fleets += [unit]

    return fleets

def get_order_loc(order: str) -> str:
    """
    Given an order, return the target of the order
    :param order: string representation of an order
    :return: string representation of a board location if included in order, else None
    """
    return OrderSplitter(order).destination

def get_owner_of_loc(loc_str: str, game:Game) -> str:
    """Get the power that owns the loc

    Args:
        loc_str (str): location string
        game (Game): Shade game engine object

    Returns:
        str: Name of the power that owns the loc
    """
    for power_name, power in game.powers.items():
        if loc_str in power.influence:
            return power_name
    return None

def get_recv_messages(power: str, game: Game)-> Dict[int, Dict[str, Any]]:
    """Get all recieved messages from a game

    Args:
        power (str): Power to get messages for
        game (Game): Diplomacy Game object

    Returns:
        Dict[int, Dict[str, Any]]: Dictionary of recieved messages
    """
    msgs = {}

    recv_msgs = Game.filter_messages(game.messages, power)

    for recv_time, message in recv_msgs.items():
        msgs[recv_time] = message.to_dict()
    
    return msgs

def get_order_type(order: str) -> str:
    """
    Given an order, return the type of the order
    :param order: string representation of an order
    :return: string representation of a board location if included in order, else None
    """
    return OrderSplitter(order).order_type
    
def get_owned_locs(game: Game, power: str) -> List[str]:
    """
    Returns a list of locations within a powers influence (as defined by SHADE game engine, to influence a location, the power must have visited it last.)
    :param game: MILA game object - encapsulates state and everything needed to play the game
    :param power: String name for a particular power (ie RUSSIA, ENGLAND, etc.)
    :return: List of string represention of locations
    """
    return game.powers[power].influence

def get_possible_orders_for_power(game: Game, power: str) -> List[str]:
    """ 
    Gets all the possible orders for a particular power
    :param game: MILA game object - encapsulates state and everything needed to play the game
    :param power: String name for a particular power (ie RUSSIA, ENGLAND, etc.)
    :return: List of string representation of orders for the given power
    """
    if power not in game.powers:
        raise ValueError("Invalid power: {}".format(power))

    orders = []

    # Get all possible orders
    all_orders = game.get_all_possible_orders()
    
    order_locs = game.get_orderable_locations(power)

    for order_loc in order_locs: 
        if order_loc in all_orders:
            orders.extend(all_orders[order_loc])

    return orders

# def get_orders_for_power_w_constraint(game: Game, power: str, constraints: list) -> List[str]:
#     """ 
#     Gets all the possible orders for a particular power, given some constraints
#     :param game: MILA game object - encapsulates state and everything needed to play the game
#     :param power: String name for a particular power (ie RUSSIA, ENGLAND, etc.)
#     :param constraints: List of locations that are not valid TODO: this is temporary, constraints are currently not specified well
#     :return: List of string representation of orders for the given power
#     """
#     if power not in game.powers:
#         raise ValueError("Invalid power: {}".format(power))

#     power_orders = []
#     # Get possible locations that a power can order
#     state_proto = state_space.extract_state_proto(game)
#     orderable_locs, power_locs = state_space.get_orderable_locs_for_powers(state_proto, [power])

#     # Get all possible orders
#     all_orders = game.get_all_possible_orders()

#     # Using orderable locs, reference and filter from all possible orders
#     for loc in power_locs[power]:
#         if loc not in constraints:
#             for order in all_orders[loc]:
#                 if get_order_loc(order) not in constraints:
#                     power_orders.append(order)

#     return power_orders

def get_attacked_powers(game: Game, orders: list) -> Set:
    """
    Gets the powers that own the locations that are being attacked
    :param game: MILA game object - encapsulates state and everything needed to play the game
    :param orders: List of string representation of orders
    :return: Set of string representation of powers
    """
    attacked_powers = set()

    all_units = game.get_units()
    all_centers = game.get_centers()

    occupied_supply = {}
    # Handle the case in which we want to take a SC that was taken by another power by spring
    for unit_power, unit in all_units.items():
        # Center belong to power A, but occupied by power B
        for center_power, center in all_centers.items():
            if get_unit_loc(unit) == center and unit_power != center_power:
                # Treat center as belongs to power B
                occupied_supply[center] = center_power

    for order in orders:
        if '-' not in order:    # Not an attack/support order
            continue
        order_loc = get_order_loc(order)

        for power_name, power in game.powers.items():
            if order_loc in occupied_supply:    # Attacking a supply center that will soon be claimed
                attacked_powers.add(occupied_supply[order_loc])
                break
            elif order_loc in power.influence:  # Attacking a location under a powers influence
                attacked_powers.add(power_name)
                break

    return attacked_powers

@dataclass(eq=True, order=True)
class Location:
    loc:str
    coast:str

    def __init__(self, loc: str, coast:str=''):
        self.loc = loc
        self.coast = coast

    def __str__(self):
        return self.to_daide_str()

    @classmethod
    def from_shade_str(cls, shade_str: str) -> Location:
        if '/' in shade_str:
            landmarks = shade_str.upper().split('/')
            try:
                coast = SHADE_2_DAIDE_COAST[landmarks[1]]
                return Location(landmarks[0], coast)
            except KeyError:
                raise ValueError("Invalid coast for location: {}".format(shade_str))     
        else:
            return Location(shade_str, '')

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Location:
        if type(daide_str) == list:
            return Location(daide_str[0], daide_str[1])
        else:
            if any([coast in daide_str for coast in DAIDE_2_SHADE_COAST.keys()]):
                contents = daide_str[daide_str.find('(')+1:daide_str.find(')')]
                contents = contents.split(' ')
                return Location(contents[0], contents[1])
            else:
                return Location(daide_str, '')

    def to_shade_str(self) -> str:
        if self.coast != '':
            return "{}/{}".format(self.loc, DAIDE_2_SHADE_COAST[self.coast])
        else:
            return self.loc

    def to_daide_str(self) -> str:
        if self.coast != '':
            return "({} {})".format(self.loc, self.coast)
        else:
            return self.loc 


class Unit:
    power:str
    loc:Location
    coast:str

    def __init__(self, power: str, loc: Location):
        self.power = power

        if type(loc) == str:
            self.loc = Location.from_daide_str(loc)
        else:
            self.loc = loc

    def __eq__(self, other):
        same_power = self.power == other.power
        same_loc = self.loc == other.loc

        return same_power and same_loc

    @classmethod
    def from_shade_str(cls, shade_str:str, power:str='000') -> Unit:
        """
        :param shade_str: string representation of unit in shade syntax
        :param game: SHADE engine game object
        :param power: If unit does not currently belong to a power (is being built), set the intended power here
        """
        components = shade_str.split(' ')
        unit_type = components[0]
        loc = Location.from_shade_str(components[1])
        
        power_name = power
        if power != '000' and len(power) > 3:
            power_name = long2short(power)

        # Return default power
        if unit_type == 'A':
            return ArmyUnit(power_name, loc)
        elif unit_type == 'F':
            return FleetUnit(power_name, loc)
        else:
            raise ValueError("Invalid unit type: {}".format(shade_str))

    @classmethod
    def from_daide_str(cls, daide_str:str) -> Unit:
        # <power> <type> <location>
        # RUS AMY MOS
        # FRA FLT (SPA NC)
        components = daide_str.replace(')', '').replace('(', '').split(' ', 3)
        if len(components) > 3:
            loc = Location(components[2], components[3])
        else:
            loc = Location(components[2])

        if components[1] == 'AMY':
            return ArmyUnit(components[0], loc)
        elif components[1] == 'FLT':
            return FleetUnit(components[0], loc) 
        else:
            raise ValueError("Invalid unit: {}".format(daide_str))

    def to_shade_str(self):
        return NotImplemented

    def to_daide_str(self):
        return NotImplemented


class ArmyUnit(Unit):
    """"""
    def __str__(self):
        return self.to_daide_str()

    def __eq__(self, other):
        same_type = isinstance(other, ArmyUnit)
        attributes = super().__eq__(other)

        return same_type and attributes

    def to_shade_str(self):
        return "A {}".format(self.loc.to_shade_str())

    def to_daide_str(self):
        return "{} AMY {}".format(self.power, self.loc)


class FleetUnit(Unit):
    """"""
    def __str__(self):
        return self.to_daide_str()

    def __eq__(self, other):
        same_type = isinstance(other, FleetUnit)
        attributes = super().__eq__(other)

        return same_type and attributes

    def to_shade_str(self):
        return "F {}".format(self.loc.to_shade_str())

    def to_daide_str(self):
        return "{} FLT {}".format(self.power, self.loc)