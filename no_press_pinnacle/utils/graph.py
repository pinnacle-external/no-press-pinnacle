from __future__ import annotations

import json

from collections import defaultdict
from typing import Dict, List, Set, Tuple

from diplomacy import Game
from diplomacy.engine.map import Map

import no_press_pinnacle.utils.mila_engine_utils as mila_utils
from no_press_pinnacle.utils.utils import create_logger, long2short

logger = create_logger('graph_utils')


class NodeEncoder(json.JSONEncoder):
    def default(self, node: Node):
        return node.__dict__


class Node:
    """
    A Node represents a province/loc on the game board/graph
    """
    _name: str              # Name of the loc
    _is_center: bool        # Has a supply center
    _land_neighbors: List[str]   # Neighbors that are land locs
    _water_neighbors: List[str]  # Neighbors that are water locs
    _owner: str             # Power that owns this loc
    _occupant: Tuple[str, str]        # The unit and owner of the unit that occupies this loc

    def __init__(self, name:str, is_center:bool, 
                 land_neighbors:list, water_neighbors: list,
                 owner:str='', occupant:tuple=('', '')):
        self._name = name
        self._is_center = is_center
        self._land_neighbors = land_neighbors
        self._water_neighbors = water_neighbors
        self._occupant = occupant
        self._owner = owner
    
    def __str__(self) -> str:
        return self._name

    def __hash__(self):
        return hash("{}_{}_{}".format(self._name, self._owner, self._occupant))

    def __eq__(self, other: Node) -> bool:
        return self._name == other._name and self._owner == other._owner

    @property
    def name(self):
        """ Name of the loc/province. """
        return self._name

    @property
    def owner(self):
        """ String representation of the power that owns this loc/province. """
        return self._owner

    @property
    def center(self):
        """ Boolean value of whether or not this loc is a supply center """
        return self._is_center

    @property
    def neighbors(self):
        """Combined land and water neighbors"""
        neighbors = set(self._water_neighbors) + set(self._land_neighbors)
        return list(neighbors)

    @property
    def occupant(self):
        """ The unit and unit owner that occupies this space, if any """
        return self._occupant

    @property
    def land_neighbors(self):
        """ Neighbors of this node that are land based. """
        return self._land_neighbors

    @property
    def water_neighbors(self):
        """ Neighbors of this node that are water based. """
        return self._water_neighbors

    def has_coast(self, other: str='') -> bool:
        """ Whether or not this loc/province has  """
        if other == '':
            return len(self._water_neighbors) > 0
        else:
            return other in self._water_neighbors

    def is_occupied(self) -> bool:
        """"""
        return self.occupant[0] != ''

    def is_neighbor(self, loc:str) -> bool:
        """"""
        return loc in self._land_neighbors or loc in self._water_neighbors

class Front:
    """ 
    A group of nodes.
    Can be nodes that are adjacent to provinces owned by an opposing power.
    """
    _locs : Set[Node] = set()
    _name : str = ''

    def __init__(self, locs:set):
        self._locs = locs
        self._update_name()

    def __str__(self) -> str:
        return self._name

    def __hash__(self):
        return hash("{}_{}".format(self._name, '-'.join(loc.name for loc in self._locs)))

    def __len__(self) -> int:
        return len(self._locs)

    def __iter__(self):
        return self

    def __next__(self):
        return next(loc for loc in self._locs)

    def __eq__(self, other: Front):
        return self._locs == other._locs

    def __add__(self, other: Front):
        return Front(self._locs | other._locs)

    def __sub__(self, other: Front):
        return Front(self._locs - other._locs)

    @property
    def locs(self):
        """ Set of nodes that are part of this front. """
        return self._locs

    @property
    def name(self):
        """ Name of this front. """
        return self._name

    def _update_name(self):
        """ Update the front name using the current locs """
        self._name = "{}_front".format('-'.join(set(loc.name for loc in self._locs)))

    def add(self, loc: Node):
        """ 
        Add a node to the front. 
        :param loc: Node to add.
        """
        self._locs.add(loc)
        self._update_name()

    def remove(self, loc: Node):
        """ 
        Remove a node from the front. 
        :param loc: Node to remove.
        """
        self._locs.remove(loc)
        self._update_name()

    def in_front(self, loc: Node):
        """ 
        Returns whether or not a node is part of this front. 
        :param loc: Node to check
        :return: boolean value
        """
        return loc in self._locs

    def get_powers_in_front(self) -> Set[str]:
        """ Returns the powers that make up the front. """
        return {loc.owner for loc in self._locs}

    def get_adjacents(self, constraints:list=[]) -> List[Node]:
        """ 
        Returns a list of nodes that are adjacent to nodes in this front.
        :param constraints: optional list of the names of provinces that should not be included
        :return: List of nodes that are adjacent to this front.
        """
        adj = set()
    
        for loc in self._locs:
            adj.update([province for province in loc.neighbors if province not in constraints])

        return adj

    def crosses_with(self, other: Front):
        """ 
        Returns whether or not another front intersects with this one
        :param other: another potentially intersecting front
        :returns: Whether there is an intersection
        """
        return len(self & other) > 0

class Graph:
    graph: Dict[Node] = {}

    def __init__(self, game: Game):
        self._graph = {}

        self.update_graph(game)

    def __len__(self) -> int:
        return len(self._graph)

    def __str__(self) -> str:
        return json.dumps(self._graph, indent=4, cls=NodeEncoder)

    def get_node(self, loc: str) -> Node:
        return self._graph[loc]

    def update_graph(self, game: Game):
        """ 
        Given a game object, update/initialize the graph.
        :param game: MILA game object - encapsulates state and everything needed to play the game
        """
        self._graph = {}
        game_map = game.map

        unit_locs = {}
        for power_name, unit_list in game.get_units().items():
            for unit in unit_list:
                unit_locs[mila_utils.get_unit_loc(unit)] = (unit, power_name)

        # Populate locations
        for loc in game_map.locs:
            adjacencies = game_map.abut_list(loc, incl_no_coast=True)

            land_neighbors, water_neighbors = [], []

            for adj in adjacencies:
                if adj in game_map.loc_type:
                    if game_map.loc_type[adj] == 'LAND':    # land neighbor only
                        land_neighbors.append(adj.upper())
                    elif game_map.loc_type[adj] == 'WATER':   # water neighbor only
                        water_neighbors.append(adj.upper())
                elif adj in game_map.loc_coasts: # both neighbors (is a COAST)
                    land_neighbors.append(adj.upper())
                    water_neighbors.append(adj.upper())

            owner = ''  # Get the owner of the location
            for power_name, power in game.powers.items():
                if loc in power.influence:
                    owner = power_name
                    break

            unit_loc = ('', '')   # Get the occupant and the associated power, if occupied
            if loc in unit_locs:
                unit_loc = unit_locs[loc]

            self._graph[loc.upper()] = Node(loc.upper(), loc in game_map.centers, 
                                    land_neighbors, water_neighbors,
                                    owner=owner, occupant=unit_loc)

    def get_owned_locs(self, power: str) -> List[Node]:
        """
        Returns the locs/provinces owned by a particular power.
        :param power: String name for a particular power (ie RUSSIA, ENGLAND, etc.)
        :return: List of nodes that are owned by the power
        """
        locs = []

        for node in self._graph.values():
            if node.owner == power or node.owner == long2short(power):
                locs.append(node)

        return locs

    def get_occupied_locs(self, power: str) -> List[Node]:
        """
        Returns the locs/provinces that contain a power's units
        :param power: String name for a particular power (ie RUSSIA, ENGLAND, etc.)
        :return: List of nodes that are contain a power's units
        """
        locs = []

        for node in self._graph.values():
            if node.occupant[1] == power or node.occupant[1] == long2short(power):
                locs.append(node)

        return locs

    def get_fronts(self, powers:List[str], alliances:List[Tuple[str]]=[]) -> Dict[str, List[Front]]:
        """
        Constructs and returns the fronts from each power's perspective.
        :param powers: List of powers
        :param allies: List of tuples that contain powers that are allies (allies share fronts) IE: [('ENG', 'AUS', 'GER'), ('RUS', 'TUR')]
        :return: Dictionary with key:value pair - power_name:list_of_fronts
        """
        fronts = defaultdict(list)
       
        # Get front from the perspective of each power
        for power in powers:
            # Get a list of locations that are occupied by a unit owned by the power
            influence = self.get_occupied_locs(power)

            # Get allies
            allies = set()
            for alliance in filter(lambda x: power in x, alliances):
                allies.update(alliance)
            for ally in allies: # Augment influence with locations occupied by ally units
                influence.extend(self.get_occupied_locs(ally))

            logger.debug("Finding fronts along: {}".format([str(loc) for loc in influence]))

            # Loop through each location in the power's influence
            for loc in influence:
                for n_name in loc.neighbors:    # Check each of the neighbors of each location
                    neighbor = self.get_node(n_name)    # Only consider neighbors that are occupied
                    if not neighbor.is_occupied():
                        continue

                    # Can be considered part of a front if the loc has an opposing unit
                    if neighbor.occupant[1] != power and neighbor.occupant[1] not in allies:
                        # Check if loc is adjacent to a loc from an already found front
                        added = False

                        # Check all
                        for front in fronts[power]:
                            is_adj_to_front = neighbor in front.get_adjacents() # candidate should be adjacent to this front
                            not_belonging = not any([f.in_front(neighbor) for f in fronts[power]]) # candidate should not belong to another front
                            same_power = neighbor.owner in front.get_powers_in_front()  # candidate should belong to the powers that make up the front
                            if is_adj_to_front and not_belonging and same_power:
                                front.add(neighbor)
                                added = True
                                logger.debug("Added {} to front {}".format(str(neighbor), str(front)))
                                break
                        # Otherwise add a new front
                        if not added:
                            new_front = Front({neighbor})
                            fronts[power].append(new_front)
                            logger.debug("Added new front {}".format(str(fronts[power][-1])))
    
        return dict(fronts)
