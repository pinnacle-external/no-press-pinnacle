import pathlib

from os import path


ROOT_DIR = pathlib.Path(path.join(path.dirname(path.dirname(path.abspath(__file__)))))

try:
    print('Importing local settings...')
    from .local_settings import *
except ImportError:
    print('No local settings detected.')