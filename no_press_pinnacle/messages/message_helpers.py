from typing import List, Tuple, Dict
from collections import defaultdict

from diplomacy.utils.splitter import OrderSplitter

from no_press_pinnacle.messages.messages import MsgOrder, And, Statement, Message
from no_press_pinnacle.utils.utils import long2short, short2long


def daide_from_dict(curr_power:str, order_dict: Dict[str, List[str]], unit_ref: Dict[str, List[str]]) -> Tuple[str, str]:
    """ 
    Convert a dictionary of orders with key/value pairs: <power>/<order_list> into DAIDE.
    Note that orders that concern the current power's own units proposed by the current power must be translated into Statements (DAIDE FCT), which cannot be nested within an AND.
    Therefore, Statements will be returned as a seperate FCT message string.
    :param curr_power: The current power issuing these messages
    :param order_dict: dictionary of orders with key/value pairs: <power>/<order_list>
    :param unit_ref: dictionary of units with key/value pairs: <power>/<unit_list>
    :returns: Tuple of size 2:
            1) Proposed orders in DAIDE syntax.
            2) Orders proposed as the current power, encapsulated in FCT, if any.
    """
    arrangements = []
    statement_list = []
    
    for power_name, order_list in order_dict.items():
        # Parse SHADE orders to construct objects
        orders = []
        for order in order_list:
            supported = OrderSplitter(order).supported_unit
            other_power = '000'
            if supported is not None:
                for p, unit_list in unit_ref.items():
                    if supported in unit_list:
                        other_power = p

            orders.append(MsgOrder.from_shade_order(order, power=long2short(power_name), other_power=other_power))

        if power_name == curr_power:
            # If the current power is proposing an order, it should be a FCT (statement) of something that will happen
            statement_list.extend(orders)
        else:        
            arrangements.extend(orders)

    # Compile all message order objects into a single And object
    arrangement = And(arrangements)
    # Compile all orders from current power into an AND, nest the AND within a FCT
    statement = Statement(And(statement_list))

    return arrangement.compose(), statement.compose()

def dict_from_daide(daide_str:str, daide_statement:str) -> Dict[str, List[str]]:
    """
    Convert a DAIDE string into a dictionary of orders.
    :param daide_str: DAIDE syntax string. NOTE: This function is fragile, it will expect specifically an AND.
    :param statements: Optional.  Use it to include a statement generated from daide_from_dict().
    :returns: dictionary of orders with key/value pairs: <power>/<order_list>.
    """

    output = defaultdict(list)

    message = Message.from_daide_str(daide_str)
    assert isinstance(message, And)

    for arrangement in message.arrangements:
        if isinstance(arrangement, MsgOrder):
            order = arrangement.order
        else:
            print("Ignoring arrangement: {}".format(arrangement))
            continue
    
        power = order.unit.power    # NOTE: will only work for orders involving a unit
        # Extract power from unit, store order in shade syntax
        output[short2long(power)].append(order.to_shade_str()) 

    # Handle statements (orders proposed by the current power)
    
    statement = Message.from_daide_str(daide_statement)
    assert isinstance(statement.arrangement, And)
    order = statement.arrangement.arrangements[0].order
    power = order.unit.power
    output[short2long(power)].append(order.to_shade_str())

    return dict(output)