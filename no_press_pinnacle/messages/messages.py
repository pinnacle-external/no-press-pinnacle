from __future__ import annotations

import json
import re
from typing import Dict, Tuple, List, Type

from no_press_pinnacle.messages.message_utils import extract_afr_parens, extract_bfr_parens, format_for_json
from no_press_pinnacle.utils.mila_engine_utils import Location
from no_press_pinnacle.messages.orders import Order

"""
PRESS messages as defined by pages 11 - 14 of daide_syntax.pdf as of 4/29/2022

Objects that make constructing and formatting DAIDE messages easier.  Can also be used as actions.
"""

# ----------------------- Utils ----------------------- # 

def class_from_token(token:str) -> Type[Message]:
    subclass_list = Message.__subclasses__()
    subclass_list.extend(Arrangement.__subclasses__())
    subclass_list.extend(CompoundArrangement.__subclasses__())
    subclass_list.extend(Reply.__subclasses__())

    for subclass in subclass_list:
        if subclass.token == token:
            return subclass
    
    raise ValueError("Invalid message token: {}".format(token))

# ----------------------- Abstract Messages ----------------------- #

class Message():
    token: str = None

    def __init__(self, contents:str):
        self.contents = contents

    def __str__(self):
        return self.contents

    def compose(self):
        return str(self)

    @classmethod
    def from_daide_str(cls, daide_str:str) -> Message:
        msg = daide_str.strip().upper() # FRM ( ENG) (FRA ITA) (PRP (ALY (ENG  FRA ITA)VSS(RUS TUR) ))
        
        # Strip away PRP if it is present
        if "PRP" in msg:
            _, msg = extract_bfr_parens(msg)

        # corrections for whitespace
        # msg = re.sub(r'\( +', r'(', msg) # ALY (ENG  FRA ITA)VSS(RUS TUR)
        # msg = re.sub(r' +\)', r')', msg) # ALY (ENG  FRA ITA)VSS(RUS TUR)
        # msg = re.sub(r' +', r' ', msg) # ALY (ENG FRA ITA) VSS (RUS TUR)

        # Extract what kind of message, and the contents
        token, contents = extract_bfr_parens(msg)

        # Get the message class
        msg_class = class_from_token(token)

        # Create and return the token
        return msg_class.from_daide_str(msg)

    def to_prolog(self) -> str:
        return 'nil'


class Arrangement(Message):
    """ A proposal to be sent to another player """
    token: str = None
    
    def __init__(self, contents:str):
        super().__init__(contents)

    def compose(self):
        return "PRP({})".format(str(self))

    @classmethod
    def from_daide_str(cls, daide_str:str) -> Arrangement:
        return Message.from_daide_str(daide_str)

class Reply(Message):
    """
    A response to a particular message proposed by another player.
    """
    token: str = None

    def __init__(self, message: Message, contents:str=''):
        super().__init__(contents)
        if not isinstance(message, Message):
            raise ValueError("Response contains invalid message: {}".format(message))

        self.message = message

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.message == other.message

    def compose(self) -> str:
        return str(self)


# ----------------------- Arrangement Operators ----------------------- #

class CompoundArrangement(Arrangement):
    token:str = None
    arrangements:List[Arrangement]

    def __init__(self, arrangements:List[Arrangement]):
        if len(arrangements) < 2:
            raise ValueError("{} must contain at least 2 other arrangements".format(self.token))
        
        for other in arrangements:    # NOTE: Check to be removed when updating to higher DAIDE level
            if not isinstance(other, Arrangement):
                raise ValueError("Cannot apply {} to: {} (is not a valid arrangement)".format(self.token, other))

        self.arrangements = arrangements

    def __str__(self):
        string = self.token
        for other in self.arrangements:
            string += "({})".format(other)
        return string

    @classmethod
    def from_daide_str(cls, daide_str: str) -> CompoundArrangement:
        # TOKEN (PRP1(...))(PRP2(...))...
        arrangement_list = []
        components = daide_str[3:]    # Slice out the token

        # Extract the first arrangement from the string -> (PRP1(...)) | (PRP2(...))(PRP3(...))...
        remaining, first = extract_afr_parens(components)
        t, _ = extract_bfr_parens(first) # Get the first token and process
        msg_cls = class_from_token(t)
        arrangement = msg_cls.from_daide_str(first) # Convert from string to object
        arrangement_list.append(arrangement)

        # Continue for each component in the compound arrangement
        while remaining is not None and remaining != '':
            remaining, raw_str = extract_afr_parens(remaining)
            t, _ = extract_bfr_parens(raw_str) # Get the next token and process
            msg_cls = class_from_token(t)
            arrangement = msg_cls.from_daide_str(raw_str)   # Convert from string to object
            arrangement_list.append(arrangement)    # Add to arrangement list

        return cls(arrangement_list)

    def compose(self) -> str:
        return super().compose()


class And(CompoundArrangement):
    """
    Arrangements can be chained using the logical AND operator.
    Cannot be nested within ORR or another AND.
    """
    token:str = "AND"

    def __eq__(self, other):
        # If not of equal length, cannot be equal
        if len(self.arrangements) != len(other.arrangements) or not isinstance(other, And):
            return False
        
        # For every term in the AND, check for equality
        return self.arrangements == other.arrangements

    @classmethod
    def from_daide_str(cls, daide_str: str) -> And:
        # AND(XDO((ENG FLT LIV) BLD))(XDO((RUS ARM MOS) MTO SEV))...

        return super().from_daide_str(daide_str)

    def to_prolog(self) -> str:
        return f"and({', '.join([arr.to_prolog() for arr in self.arrangements])})"

class Not(Arrangement):
    """ Inverse operator for arrangements. """
    token:str = "NOT"

    def __init__(self, arrangement:Arrangement):
        if not isinstance(arrangement, Arrangement):
            raise ValueError("Cannot apply NOT to: {}".format(arrangement))

        self.arrangement = arrangement

    def __eq__(self, other):
        if not isinstance(other, Not):
            return False

        return self.arrangement == other.arrangement

    def __str__(self):
        return "NOT({})".format(str(self.arrangement))

    @classmethod    
    def from_daide_str(cls, daide_str: str) -> Not:
        # NOT(PRP1(...))
        token, contents = extract_bfr_parens(daide_str)

        return Not(Message.from_daide_str(contents))

class Orr(CompoundArrangement):
    """
    ORR (group of arrangements, to be rejected fully and/or responded to with portions that are to be accepted)
    """
    token:str = "ORR"

    def __eq__(self, other):
        if not isinstance(other, Orr):
            return False

        # If not of equal length, cannot be equal
        if len(self.arrangements) != len(other.arrangements) or not isinstance(other, Orr):
            return False

        # For every term in the ORR, check for equality
        return self.arrangements == other.arrangements
    
    @classmethod
    def from_daide_str(cls, daide_str: str) -> Orr:
        # ORR(PRP1(...))(PRP2(...) ...)

        return super().from_daide_str(daide_str)

    def to_prolog(self) -> str:
        return f"or({' '.join([arr.to_prolog() for arr in self.arrangements])})"

# ----------------------- Arrangements ----------------------- #

class Draw(Arrangement):
    """ Propose a draw """
    token:str = "DRW"
    def __init__(self):
        pass

    def __str__(self):
        return "DRW"

    def __eq__(self, other):
        return isinstance(other, Draw)
    
    @classmethod
    def from_daide_str(cls, daide_str: str) -> Draw:
        # [DRW]
        return Draw()

class Ally(Arrangement):
    """
    Arrange an Alliance between the powers in the first list. The second list is the powers to ally against. 
    Eliminated powers must not be included in either power list. The arrangement is continuous (i.e. it isn't just for the current turn).
    """
    token:str = "ALY"

    def __init__(self, allies: list, enemies: list):
        if len(allies) == 0:
            raise ValueError("Cannot propose an alliance with no other allies.")
        if len(enemies) == 0:
            raise ValueError("Cannot propose an alliance without any enemies to ally against.")

        self.allies = allies
        self.enemies = enemies

    def __eq__(self, other):
        if not isinstance(other, Ally):
            return False

        if len(self.allies) != len(other.allies) or len(self.enemies) != len(other.enemies):
            return False

        return self.allies == other.allies and self.enemies == other.enemies

    def __str__(self):
        allies = " ".join(self.allies)
        enemies = " ".join(self.enemies)
        return "ALY({})VSS({})".format(allies, enemies)

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Ally:
        # ALY(ally1 ally2...) VSS(enemy1 enemy2...)
        parts = daide_str.split('VSS')

        # Remove the ALY token and convert to list
        allies = format_for_json(parts[0][3:])
        allies = json.loads(allies)
        
        # Convert to list
        enemies = format_for_json(parts[1])
        enemies = json.loads(enemies)

        return Ally(allies, enemies)

    def to_prolog(self):
        return f"alliance({self.allies}, {self.enemies})"

class Peace(Arrangement):
    """
    Arrange Peace between the listed powers. 
    Eliminated powers must not be included in the power list. 
    The arrangement is continuous.
    """
    token:str = "PCE"

    def __init__(self, powers: list):
        if len(powers) < 1:
            raise ValueError("Cannot have a PCE with no powers involved.")
        self.powers = powers
        self.powers.sort()

    def __eq__(self, other):
        if not isinstance(other, Peace):
            return False

        return self.powers == other.powers

    def __str__(self):
        powers = " ".join(self.powers)
        return "PCE({})".format(powers)

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Peace:
        # PCE(power1 power2 ...)
        content = daide_str[3:]
        powers = json.loads(format_for_json(content))

        return Peace(powers)

class Solo(Arrangement):
    """
    Propose a Solo to the specified power. 
    Note that the client can't actually order a solo - but may be able to order its units in a way that causes one to occur.
    """
    token:str = "SLO"

    def __init__(self, power: str):
        self.power = power

    def __eq__(self, other):
        if not isinstance(other, Solo):
            return False

        return self.power == other.power

    def __str__(self):
        return "SLO({})".format(self.power)

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Solo:
        # SLO(power)
        token, content = extract_bfr_parens(daide_str)
        return Solo(content)

class MsgOrder(Arrangement):
    """ Propose an order to be executed the next phase when it is valid. """
    token:str = "XDO"
    
    def __init__(self, order:Order):
        self.order = order
    
    def __eq__(self, other):
        if not isinstance(other, MsgOrder):
            return False

        return self.order == other.order

    def __str__(self):
        return "XDO({})".format(str(self.order)) 

    @classmethod
    def from_daide_str(cls, daide_str: str) -> MsgOrder:
        # XDO(order)
        token, content = extract_bfr_parens(daide_str)
        return MsgOrder(Order.from_daide_str(content))

    @classmethod
    def from_shade_order(cls, shade_str:str, 
                         power:str='000', other_power:str='000', 
                         path:List[str]=[]) -> MsgOrder:
        """
        Convenience function that, given a single shade order (ie F IRI - MAO), constructs a MsgObject 
        :param shade_str: the order in SHADE engine syntax
        :param power: If the order is known to reference a unit, set its power via this parameter
        :param other_power: If the order is known to reference a secondary unit, set its power via this parameter  
        :param path: If the order is known to be a 'move by convoy' use this parameter to add the path
        """
        order = Order.from_shade_str(shade_str, power, other_power, path)

        return MsgOrder(order)

    def to_prolog(self) -> str:
        return self.order.to_prolog()


class DMZ(Arrangement):
    """
    This is an arrangement for the listed powers to remove all units from, and not order to, support to, convoy to, retreat to, or build any units in any of the list of provinces. 
    Eliminated powers must not be included in the power list. The arrangement is continuous
    """
    token:str = "DMZ"

    def __init__(self, powers: list, locs: List[Location]):
        self.powers = powers
        self.locs = locs
        
        self.powers.sort()
        self.locs.sort()

    def __eq__(self, other):
        if not isinstance(other, DMZ):
            return False

        # If not of equal length, cannot be equal
        if len(self.powers) != len(other.powers) or len(self.locs) != len(other.locs):
            return False

        return self.powers == other.powers and self.locs == other.locs

    def __str__(self):
        powers = " ".join(self.powers)
        locs = " ".join([str(loc) for loc in self.locs])
        return "DMZ({})({})".format(powers, locs)   

    @classmethod
    def from_daide_str(cls, daide_str: str) -> DMZ:
        # DMZ(power1 power2 ...)(loc1 loc2 ...)
        contents, powers = extract_afr_parens(daide_str)        
        # separate from DMZ token, reformat to conver to list 
        powers = powers.strip().split(' ')
        
        _, raw_locs = extract_afr_parens(contents)
        raw_locs = format_for_json('(' + raw_locs.strip() + ')')
        locs = [Location.from_daide_str(loc) for loc in json.loads(raw_locs)]

        return DMZ(powers, locs) 

# ----------------------- Responses ----------------------- #

class Yes(Reply):
    """
    Respond to a proposed message with a 'Yes'
    """
    token:str = "YES"

    def __str__(self):
        return "YES({})".format(str(self.message))

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Yes:
        # YES(proposal)
        token, content = extract_bfr_parens(daide_str)
        return Yes(Message.from_daide_str(content))

class Reject(Reply):
    """
    Respond to a proposed message with 'No'
    """
    token:str = "REJ"

    def __str__(self):
        return "REJ({})".format(str(self.message))

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Reject:
        # REJ(proposal)
        token, content = extract_bfr_parens(daide_str)
        return Reject(Message.from_daide_str(content))

class Beeswax(Reply):
    """ Respond with a 'refuse to respond' """
    token:str = "BSX"

    def __str__(self):
        return "BSX({})".format(str(self.message))

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Beeswax:
        # BSX(proposal)
        token, content = extract_bfr_parens(daide_str)
        return Beeswax(Message.from_daide_str(content))

class Huh(Reply):
    """ Respond with a 'I do not understand this message' """
    token:str = "HUH"

    def __str__(self):
        return "HUH({})".format(str(self.message))

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Huh:
        # HUH(proposal)
        token, content = extract_bfr_parens(daide_str)
        return Huh(Message.from_daide_str(content))

# ----------------------- Utility ----------------------- #

class Try(Message):
    """ Used as a followup for HUH, include possible alternatives that the agent will understand. """
    token:str = "TRY"
    
    def __init__(self, options):
        """
        :param options: is a series of strings that represent the alternatives that the agent is able to respond to.
        """
        self.options = options
        self.options.sort()

    def __eq__(self, other):
        if not isinstance(other, Try) or len(self.options) != len(other.options):
            return False
        return self.options == other.options

    def __str__(self):
        return "TRY({})".format(" ".join(self.options))

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Try:
        # TRY(token1 token2 ...)
        token, content = extract_bfr_parens(daide_str)
        help_tokens = json.loads(format_for_json(content))
        return Try(help_tokens)

class Cancel(Message):
    """ Send a message to cancel a previously proposed but not yet accepted message. """
    token:str = "CCL"
    
    def __init__(self, message: Message):
        if not isinstance(message, Message):
            raise ValueError("Cancel contains invalid message: {}".format(message))

        self.message = message

    def __eq__(self, other):
        if not isinstance(other, Cancel):
            return False

        return self.message == other.message

    def __str__(self):
        return "CCL({})".format(str(self.message))

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Cancel:
        # CCL(proposal)
        token, content = extract_bfr_parens(daide_str)
        return Cancel(Message.from_daide_str(content))

class Statement(Message):
    """
    This is generally used either to pass information, or to unilaterally override a previously made agreement. 
    There are no defined replies to FCT, although of course, a FCT message may cause other statements or proposals to follow.
    """
    token:str = "FCT"

    def __init__(self, arrangement: Arrangement):
        self.arrangement = arrangement

    def __eq__(self, other):
        if not isinstance(other, Statement):
            return False

        return self.arrangement == other.arrangement

    def __str__(self):
        return "FCT({})".format(str(self.arrangement))

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Statement:
        # FCT(proposal)
        token, content = extract_bfr_parens(daide_str)
        return Statement(Arrangement.from_daide_str(content))
