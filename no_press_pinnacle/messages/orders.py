from __future__ import annotations

import re
from typing import Tuple, List, Type

from diplomacy.utils.splitter import OrderSplitter
from diplomacy import Game

from no_press_pinnacle.utils.mila_engine_utils import Unit, ArmyUnit, FleetUnit, Location, get_all_fleets
from no_press_pinnacle.messages.message_utils import extract_afr_parens
from no_press_pinnacle.utils import long2short, get_unit_owner, get_unit_loc, get_owner_of_loc, short2long

# ----------------------- Orders ----------------------- #
# TODO: Make this framework compatible when communicating about hypothetical units that have not been built yet?

class Order():
    """"""
    tokens:List = None

    @classmethod
    def from_shade_str_w_power(cls, shade_str:str, game:Game) -> Order:
        order = OrderSplitter(shade_str)
        power = '000'
        other_power = '000'
        path = []

        if order.unit is not None:
            # if build phase and unit is located in a supply center, return power associated with that supply center
            unit_loc = get_unit_loc(order.unit).split('/')[0]
            if game.phase_type == 'A' and order.order_type == 'B':
                assert any([(unit_loc in p.centers) for p in game.powers.values()]), f"Got invalid order {order.order_type} during adjustments on non-supply center loc {unit_loc} (order was {shade_str})"
                power = get_owner_of_loc(unit_loc, game)
                if power is None:
                    raise ValueError(f"Unable to get the owner of the unit {order.unit}")
            else:   
                power = get_unit_owner(order.unit, game)
        
            # This is a move by convoy - need to calculate path
            if order.destination == "VIA":
                start = unit_loc
                dest = shade_str.split(' ')[3]   # Fix dest issue by splitting <unit> - <dest> VIA
                convoying_units = get_all_fleets(game)
                fleets = {loc[2:] for loc in convoying_units}
                possible_paths = [path for path in game.convoy_paths_dest.get(start, {}).get(dest, set([])) if path.issubset(fleets)]
                possible_paths = [[start] + list(path) + [dest] for path in possible_paths]
                if len(possible_paths) > 0:
                    path = possible_paths[0]    # TODO: path to take will matter in some situations!
                    path = [Location.from_shade_str(p) for p in path]   # Convert to Location object

        if order.supported_unit is not None:
            other_power = get_unit_owner(order.supported_unit, game)

        return cls.from_shade_str(shade_str, power=power, other_power=other_power, path=path)


    @classmethod
    def from_shade_str(cls, shade_str:str, power:str='000', other_power:str='000', path:List[Location]=[]) -> Order:
        """
        :param shade_str: the order in SHADE engine syntax
        :param power: If the order is known to reference a unit, set its power via this parameter
        :param other_power: If the order is known to reference a secondary unit, set its power via this parameter  
        :param path: If the order is known to be a 'move by convoy' use this parameter to add the path
        """
        order = OrderSplitter(shade_str)

        dest = None
        if order.destination is not None:
            dest = Location.from_shade_str(order.destination)
        unit = None

        power_name = power
        if power_name is not None and len(power_name) > 3:
            power_name = long2short(power_name)

        other_power_name = other_power
        if other_power_name is not None and len(other_power_name) > 3:
            other_power_name = long2short(other_power_name)

        if order.unit is not None:
            unit = Unit.from_shade_str(order.unit, power=power_name) # Construct Unit

        if order.destination == 'VIA':  # Move by convoy    NOTE: there may be a bug in the splitter that leaves VIA as the destination
            components = shade_str.split(' ')   # Fix dest issue by splitting <unit> - <dest> VIA
            dest = Location.from_shade_str(components[3])
            return MoveByConvoy(unit, dest, path)
        elif order.order_type == 'H': # Hold
            return Hold(unit)
        elif order.order_type == '-': # Move
            return Move(unit, dest)
        elif order.order_type == 'S': 
            supported_unit = Unit.from_shade_str(order.supported_unit, power=other_power_name)
            if order.support_order_type == '-': # SupportMove
                return SupportMove(unit, supported_unit, dest) 
            else:   # SupportHold   
                #TODO: May need a different default
                return SupportHold(unit, supported_unit)
        elif order.order_type == 'C':   # Convoy
            # NOTE: Convoy order has fleet come first, then army
            convoyed_unit = Unit.from_shade_str(order.supported_unit, power=other_power)
            if order.support_order_type == '-': # Convoy
                return Convoy(convoyed_unit, unit, dest)
            else:
                raise ValueError("Invalid shade order for convoy: {}".format(shade_str))
        elif order.order_type == 'R':   # Retreat
            return Retreat(unit, dest)
        elif order.order_type == 'B':   # Build
            return Build(unit)
        elif order.order_type == 'D':   # Disband/Remove
            return Remove(unit)
        elif order.order_type == 'WAIVE':   # Waive Build
            return WaiveBuild(power)
        else:
            raise ValueError("Invalid string: {}".format(shade_str))

    @classmethod
    def from_daide_str(cls, daide_str:str) -> Order:
        components = daide_str.split(' ')

        order_classes = Order.__subclasses__()
        order_classes.extend(DestinationOrder.__subclasses__())
        order_classes.extend(UnitOrder.__subclasses__())

        # Prioritize classes with more tokens (there are overlaps with classes with single tokens)
        order_classes.sort(key=lambda x: len(x.tokens) if x.tokens is not None else 100, reverse=True)

        for order_cls in order_classes:
            if order_cls.tokens is None:    # Skip over abstract classes
                continue
            
            # Check if all tokens exist within the given order
            if all([token in components for token in order_cls.tokens]):
                return order_cls.from_daide_str(daide_str)
        
        raise ValueError("Invalid order: {}".format(daide_str))

    def to_shade_str(self):
        raise NotImplementedError

    def to_prolog(self) -> str:
        return "nil"


class UnitOrder(Order):
    unit: Unit
    tokens:List = None
    def __init__(self, unit:Unit):
        self.unit = unit

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.unit == other.unit


class DestinationOrder(UnitOrder):
    dest: Location
    tokens:List = None
    def __init__(self, unit:Unit, dest: Location):
        super().__init__(unit)
        self.dest = dest

    def __eq__(self, other):
        same_type = isinstance(other, self.__class__)
        same_unit = self.unit == other.unit
        same_dest = self.dest == other.dest

        return same_type and same_unit and same_dest


class Hold(UnitOrder):
    """"""
    tokens:List[str] = ["HLD"]

    def __str__(self):
        return "({}) HLD".format(self.unit.to_daide_str())

    def to_shade_str(self):
        # unit_type unit_loc H
        return "{} H".format(self.unit.to_shade_str())

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Hold:
        # (unit) HLD
        _, unit = extract_afr_parens(daide_str)
        return Hold(Unit.from_daide_str(unit))

    def to_prolog(self) -> str:
        unit_type = "a" if isinstance(self.unit, ArmyUnit) else "f"
        return f"hold({short2long(self.unit.power)}, {unit_type}, {self.unit.loc.to_shade_str()})".lower()


class Move(DestinationOrder):
    """"""
    tokens:List[str] = ["MTO"]

    def __str__(self):
        return "({}) MTO {}".format(self.unit.to_daide_str(), self.dest)

    def to_shade_str(self):
        # unit_type unit_loc - dest
        return "{} - {}".format(self.unit.to_shade_str(), self.dest.to_shade_str())

    @classmethod
    def from_daide_str(cls, daide_str:str) -> Move:
        # (unit) MTO destination
        components, unit = extract_afr_parens(daide_str)
        components = components.strip().split('MTO')
        dest = Location.from_daide_str(components[1].strip())

        return Move(Unit.from_daide_str(unit), dest)

    def to_prolog(self) -> str:
        unit_type = "a" if isinstance(self.unit, ArmyUnit) else "f"
        return f"move({short2long(self.unit.power)}, {unit_type}, {self.unit.loc.to_shade_str()}, {self.dest.to_shade_str()})".lower()


class SupportHold(UnitOrder):
    """"""
    tokens:List[str] = ["SUP"]
    
    def __init__(self, unit:Unit, target:Unit):
        super().__init__(unit)
        self.target = target

    def __eq__(self, other):
        same_type = isinstance(other, SupportHold)
        same_unit = self.unit == other.unit
        same_target = self.target == other.target
        return same_type and same_unit and same_target

    def __str__(self):
        return "({}) SUP ({})".format(self.unit, self.target)

    def to_shade_str(self):
        # unit_type unit_loc S target_type target_loc
        return "{} S {}".format(self.unit.to_shade_str(), self.target.to_shade_str())

    @classmethod
    def from_daide_str(cls, daide_str:str) -> SupportHold:
        # (unit) SUP (target)
        components, unit_str = extract_afr_parens(daide_str)
        components = components.strip()
        components = components.split(' ', 1)
        _, target_str = extract_afr_parens(components[1].strip())

        unit = Unit.from_daide_str(unit_str)
        target = Unit.from_daide_str(target_str)

        return SupportHold(unit, target)

    def to_prolog(self) -> str:
        unit_type1 = "a" if isinstance(self.unit, ArmyUnit) else "f"
        unit_type2 = "a" if isinstance(self.target, ArmyUnit) else "f"
        return f"support(hold, {short2long(self.unit.power)}, {unit_type1}, {self.unit.loc.to_shade_str()}, {self.target.power}, {unit_type2}, {self.target.loc.to_shade_str()})".lower()

class SupportMove(DestinationOrder):
    """"""
    target:Unit
    tokens:List[str] = ["SUP", "MTO"]

    def __init__(self, unit:Unit, target:Unit, dest:str):
        super().__init__(unit, dest)
        self.target = target

    def __eq__(self, other):
        same_type = isinstance(other, SupportMove)
        same_unit = self.unit == other.unit
        same_target = self.target == other.target
        same_dest = self.dest == other.dest

        return same_type and same_unit and same_target and same_dest

    def __str__(self):
        return "({}) SUP ({}) MTO {}".format(self.unit.to_daide_str(), self.target.to_daide_str(), self.dest)

    def to_shade_str(self):
        # unit_type unit_loc S target_type target_loc - dest
        return "{} S {} - {}".format(self.unit.to_shade_str(), self.target.to_shade_str(), self.dest.to_shade_str())

    @classmethod
    def from_daide_str(cls, daide_str:str) -> SupportMove:
        # (unit) SUP (target) MTO destination
        components, unit = extract_afr_parens(daide_str)    # Get unit
        components = components.strip()
        components, target = extract_afr_parens(components) # Get target
        components = components.strip().split(' ', 1)
        dest = Location.from_daide_str(components[1].strip())    # Get destination

        return SupportMove(Unit.from_daide_str(unit), Unit.from_daide_str(target), dest)

    def to_prolog(self) -> str:
        unit_type1 = "a" if isinstance(self.unit, ArmyUnit) else "f"
        unit_type2 = "a" if isinstance(self.target, ArmyUnit) else "f"
        return f"support(move, {short2long(self.unit.power)}, {unit_type1}, {self.unit.loc.to_shade_str()}, {self.target.power}, {unit_type2}, {self.target.loc.to_shade_str()}, {self.dest.to_shade_str()})".lower()


class Convoy(DestinationOrder):
    """"""
    ship:Unit
    tokens:List[str] = ["CVY", "CTO"]

    def __init__(self, unit:Unit, ship:FleetUnit, dest:str):
        if not isinstance(unit, ArmyUnit):
            raise ValueError("Fleets cannot convoy a non-army unit: {} ({})".format(unit, type(unit)))
        elif not isinstance(ship, FleetUnit):
            raise ValueError("Cannot convoy using non-fleet unit: {} ({})".format(ship, type(ship)))
        super().__init__(unit, dest)
        self.ship = ship

    def __eq__(self, other):
        same_type = isinstance(other, Convoy)
        same_unit = self.unit == other.unit
        same_ship = self.ship == other.ship
        same_dest = self.dest == other.dest

        return same_type and same_unit and same_ship and same_dest

    def __str__(self):
        return "({}) CVY ({}) CTO {}".format(self.ship.to_daide_str(), self.unit.to_daide_str(), self.dest)

    def to_shade_str(self):
        # F unit_loc C A target_loc - dest
        return "{} C {} - {}".format(self.ship.to_shade_str(), self.unit.to_shade_str(), self.dest.to_shade_str())

    @classmethod
    def from_daide_str(cls, daide_str:str) -> Convoy:
        # (ship) CVY (unit) CTO destination
        components, ship = extract_afr_parens(daide_str)    # Get ship
        components, unit = extract_afr_parens(components.strip()) # Get army
        components = components.strip().split('CTO')
        dest = Location.from_daide_str(components[1].strip())    # Get destination

        return Convoy(Unit.from_daide_str(unit), Unit.from_daide_str(ship), dest)

    def to_prolog(self) -> str:
        unit_type1 = "a" if isinstance(self.unit, ArmyUnit) else "f"
        unit_type2 = "a" if isinstance(self.ship, ArmyUnit) else "f"
        return f"convoy_to({short2long(self.unit.power)}, {unit_type1}, {self.unit.loc.to_shade_str()}, {self.ship.power}, {unit_type2}, {self.ship.loc.to_shade_str()}, {self.dest.to_shade_str()})".lower()


class MoveByConvoy(DestinationOrder):
    """"""
    tokens:List[str] = ["CTO", "VIA"]
    path:List[Location]

    def __init__(self, unit: Unit, dest: Location, path:List[Location]):
        if not isinstance(unit, ArmyUnit):
            raise ValueError("Invalid MoveByConvoy, must be ArmyUnit: {}".format(unit))

        super().__init__(unit, dest)
        self.path = path

    def __str__(self):
        return "({}) CTO {} VIA ({})".format(self.unit.to_daide_str(), self.dest, " ".join([str(l) for l in self.path]))

    def __eq__(self, other):
        same_type = isinstance(other, MoveByConvoy)
        same_unit = self.unit == other.unit
        same_dest = self.dest == other.dest
        same_path = self.path == other.path

        return same_type and same_unit and same_dest and same_path 

    def to_shade_str(self):
        # NOTE: Shade game engine auto-calculates convoy routes?  May need to recheck this
        # A unit_type - dest VIA
        return "{} - {} VIA".format(self.unit.to_shade_str(), self.dest.to_shade_str())

    @classmethod
    def from_daide_str(cls, daide_str:str) -> MoveByConvoy:
        # (unit) CTO dest VIA (loc1 loc2 loc3...)
        components, unit = extract_afr_parens(daide_str)    # Get the unit
        dest = Location.from_daide_str(components.split('CTO')[1].split('VIA')[0].strip()) # Get the destination

        # Get the locations
        locations = components[components.find('(')+1:components.rfind(')')]
        locations = [Location.from_daide_str(loc.strip()) for loc in re.split(r'\s+(?=[^()]*\))', locations)]

        return MoveByConvoy(Unit.from_daide_str(unit), dest, locations)

    def to_prolog(self) -> str:
        unit_type = "a" if isinstance(self.unit, ArmyUnit) else "f"
        return f"convoy_via({short2long(self.unit.power)}, {unit_type}, {self.unit.loc.to_shade_str()}, {self.dest.to_shade_str()}, {[loc.to_shade_str() for loc in self.path]})".lower()


class Retreat(DestinationOrder):
    """"""
    tokens:List[str] = ["RTO"]

    def __str__(self):
        return "({}) RTO {}".format(self.unit.to_daide_str(), self.dest)

    def to_shade_str(self):
        # unit_type unit_loc R dest
        return "{} R {}".format(self.unit.to_shade_str(), self.dest.to_shade_str())

    @classmethod
    def from_daide_str(cls, daide_str:str) -> Retreat:
        # (unit) RTO destination
        components, unit = extract_afr_parens(daide_str)
        components = components.split('RTO')
        dest = Location.from_daide_str(components[1].strip())

        return Retreat(Unit.from_daide_str(unit), dest)

    def to_prolog(self) -> str:
        unit_type = "a" if isinstance(self.unit, ArmyUnit) else "f"
        return f"retreat({short2long(self.unit.power)}, {unit_type}, {self.unit.loc.to_shade_str()}, {self.dest.to_shade_str()})"


# NOTE: Not implemented in SHADE engine?
# class RetreatFromBoard(UnitOrder):
#     """"""
#     tokens:List = ("DSB")

#     def __str__(self):
#         return "({}) DSB".format(self.unit.to_daide_str())

#     def to_shade_str(self):
#         # 
#         return NotImplemented # "{}".format() 


class Build(UnitOrder):
    """"""
    tokens:List[str] = ["BLD"]

    def __str__(self):
        return "({}) BLD".format(self.unit.to_daide_str())

    def to_shade_str(self):
        # unit_type unit_loc B
        return "{} B".format(self.unit.to_shade_str())

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Build:
        # (unit) BLD
        _, unit = extract_afr_parens(daide_str)
        return Build(Unit.from_daide_str(unit))
    
    def to_prolog(self) -> str:
        unit_type = "a" if isinstance(self.unit, ArmyUnit) else "f"
        return f"build({short2long(self.unit.power)}, {unit_type}, {self.unit.loc.to_shade_str()})".lower()


class Remove(UnitOrder):
    """"""
    tokens:List[str] = ["REM"]

    def __str__(self):
        return "({}) REM".format(self.unit.to_daide_str())

    def to_shade_str(self):
        # NOTE: Implemented in shade engine as Disband
        # unit_type unit_loc D
        return "{} D".format(self.unit.to_shade_str())

    @classmethod
    def from_daide_str(cls, daide_str: str) -> Remove:
        # (unit) REM
        _, unit = extract_afr_parens(daide_str)
        return Remove(Unit.from_daide_str(unit))

    def to_prolog(self) -> str:
        unit_type = "a" if isinstance(self.unit, ArmyUnit) else "f"
        return f"remove_unit({short2long(self.unit.power)}, {unit_type}, {self.unit.loc.to_shade_str()})"


class WaiveBuild(Order):
    """"""
    tokens:List[str] = ["WVE"]

    def __init__(self, power: str):
        self.power = power

    def __str__(self):
        return "{} WVE".format(self.power)

    def __eq__(self, other):
        return self.power == other.power

    def to_shade_str(self):
        return "WAIVE"

    @classmethod
    def from_daide_str(cls, daide_str:str) -> WaiveBuild:
        components = daide_str.split(' ')
        return WaiveBuild(components[0].strip())

    def to_prolog(self) -> str:
        return f"waive({short2long(self.power)})"