import re
from typing import Tuple

from diplomacy import Game
from no_press_pinnacle.utils.utils import long2short


# def get_parens_contents(string: str) -> str:



def extract_bfr_parens(string: str) -> Tuple[str, str]:
    """
    :return: Tuple:
        1) string before parenthesis
        2) string contained within parenthesis
    """
    token = string[:3].strip()  # get the three letter token from the front
    contents = string[3:].strip()
    last_paren_index = contents.rfind(')')
    contents = contents[contents.find('(')+1:last_paren_index].strip()
    return token, contents

def extract_afr_parens(string: str) -> Tuple[str, str]:
    """
    :return: Tuple:
        1) string after parenthesis
        2) string contained within parenthesis
    """
    open_count = 0
    start_index = None
    after_index = 0
    for index, char in enumerate(string):
        if char == '(':
            if start_index is None:
                start_index = index + 1
            open_count += 1
        elif char == ')':
            open_count -= 1
            if open_count == 0:
                after_index = index
                break

    if start_index is None:
        raise ValueError("Invalid string - no opening parenthesis: {}".format(string))

    after = string[after_index+1:].strip()
    contents = string[start_index:after_index].strip()

    return after, contents

def format_for_json(string: str) -> str:
    """ Prepare a message string to be turned into json """
    # Remove beginning and end whitespace if present
    output = string.strip() 
    output = re.sub(r'\( +', r'(', output)
    output = re.sub(r' +\)', r')', output)
    # Add whitespace where relevant
    output = re.sub(r'([A-Z])\(', r'\1 (', output)
    output = re.sub(r'\)([A-Z])', r') \1', output)
    output = re.sub(r' +', r' ', output)
    output = re.sub(r'\)\(', r') (', output)
    # Add quotes around each term
    output = re.sub(r'([A-Z]{3})', r'"\1"', output)
    # Replace whitespace delimiters with ','
    output = re.sub(r' ', r', ', output)
    # Replace () with []
    output = re.sub(r'\(', r'[', output)
    output = re.sub(r'\)', r']', output)

    return output

def daide_to_shade_unit(unit: str) -> str:
    """
    Convert a unit from DAIDE syntax to shade engine syntax
    ie: DAIDE: ENG AMY LVP to shade: A LVP
    """
    components = unit.split(' ')
    if components[1] == 'AMY':
        return "A {}".format(components[2])
    elif components[1] == 'FLT':
        return "F {}".format(components[2])
    else:
        raise ValueError("Invalid unit: {}".format(unit))


def shade_to_daide_unit(unit:str, game: Game, power:str=None):
    """
    Convert a unit from shade engine syntax to DAIDE syntax
    ie: shade: A LVP to DAIDE: ENG AMY LVP
    """
    components = unit.split(' ')
    unit_type = components[0]
    loc = components[1].upper()
    power_name = power
    if power is None:
        power_name = get_shade_unit_power(unit, game)

    if unit_type == 'A':
        return "{} AMY {}".format(long2short(power_name), loc)
    elif unit_type == 'F':
        return "{} FLT {}".format(long2short(power_name), loc)
    else:
        raise ValueError("Invalid unit type: {}".format(unit))

def get_shade_unit_power(unit: str, game: Game) -> str:
    components = unit.split(' ')
    loc = components[1].upper()

    for power_name, p in game.powers.items():
        if loc in p.units:
            return power_name
        
    return None
