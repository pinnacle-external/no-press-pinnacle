from diplomacy import Game

class NoPressInterface:
    """
    Generic NO-PRESS interface
    """
    def __init__(self, game:Game=None):
        if game is None:
            self.game = Game()
        else: 
            self.game = game

    def get_game(self) -> Game:
        """
        Returns a the Game object
        :returns: Game object 
        """
        return self.game

    def update_game(self, game: Game):
        """
        Updates the internal Game object to reflect a new state
        """
        self.game = game