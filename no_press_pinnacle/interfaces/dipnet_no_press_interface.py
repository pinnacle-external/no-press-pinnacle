import os
from collections import defaultdict
from copy import deepcopy
from typing import Dict, List, Tuple, Set

import no_press_pinnacle.utils.mila_engine_utils as mila_utils
from diplomacy import Game
# from diplomacy_research.players.benchmark_player import DipNetRLPlayer
from no_press_pinnacle.interfaces.no_press_interface import NoPressInterface
# from tornado import gen


class DipNetNoPressInterface(NoPressInterface):
    def __init__(self, game: Game):
        """ Interface for using the DIPNET agent within the Pinnacle NOPRESS module """
        super().__init__(game)
        
        # # Dipnet requires the WORKING_DIR environment variable to be set in order to download the model + singularity container
        # self.player = DipNetRLPlayer()
        raise NotImplementedError

    # def _does_attack_constraint(self, order: str, constraints:list=[]) -> bool:
    #     """ 
    #     Utility to check if the order is an attack and violates a constraint
    #     :param order: string representation of an order
    #     :param ally_influences: list of string representations of locations of allied influences
    #     :return: boolean value of whether or not the order is an attack and violates a constraint
    #     """
    #     if '-' not in mila_utils.get_order_type(order):
    #         return False

    #     return mila_utils.get_order_loc(order) in constraints

    # def _merge_forces(self, game: Game, power: str, powers: list=[]):
    #     """
    #     Performs a merge operation on all listed powers, with the specified power being the recipient of all of the listed power's assets.
    #     Utilizes the diplomacy game engine's Power.merge() function.
    #     :param game: Game object
    #     :param power: String representation of the target power.  This power will recieve all assets of the listed powers (supply centers, armys, fleets, etc.)
    #     :param powers: List of string representations of powers.  These powers will lose all of their assets.
    #     :returns: tuple of size 2:
    #         1) game object, after the merge operation has been done.
    #         2) list of locations owned by the collective powers, to be used as constraints
    #     """

    #     with game.current_state():
    #         for p in powers:
    #             game.powers[power].merge(game.powers[p])

    #         # Owned locs per power, use to filter (no attacks on allied locations)
    #         owned_locs = [mila_utils.get_owned_locs(game, ally) for ally in powers]
    #         owned_locs.append(mila_utils.get_owned_locs(game, power))
        
    #         return game, owned_locs

    # @gen.coroutine
    # def get_orders(self, power: str) -> List:
    #     """
    #     Wrapper function for DipNet's get_orders() function.  Given a power, returns a list of orders.
    #     :param power: String name for a particular power (ie RUS, ENG, etc.).
    #     :return: List of string representation of orders.
    #     """
    #     with self.game.current_state():
    #         orders = yield self.player.get_orders(self.game, power)
    #     return orders

    # @gen.coroutine
    # def get_beam_orders(self, power: str, constraints: list = []) -> Tuple[List, List]:
    #     """ 
    #     Wrapper function for DipNet's get_beam_orders() function.  Given a power, returns a list of sets of orders along with probabilities that match each set of orders. 
    #     :param power: String name for a particular power (ie RUS, ENG, etc.).
    #     :param constraints: List of locations that are not valid TODO: this is temporary, constraints are currently not specified well
    #     :return: tuple of size 2: 
    #         1) list of string representation of orders for the given power
    #         2) list of beam probabilities corresponding to orders
    #     """

    #     with self.game.current_state():
    #         beam_orders = yield self.player.get_beam_orders(self.game, power)

    #         if len(constraints) == 0:   # Early exit - no constraints
    #             return beam_orders

    #         order_list = []
    #         prob_list = []

    #         # Filter out orders that violate constraints
    #         for order_pair in beam_orders:
    #             orders = order_pair[0]
    #             probs = order_pair[1]

    #             violates_constraint = False
    #             for order in orders:
    #                 # If order violates any of the constraints, 
    #                 if mila_utils.get_order_loc(order) in constraints:
    #                     violates_constraint = True
    #                     break 

    #             if not violates_constraint:
    #                 order_list.append(orders)
    #                 prob_list.append(probs)

    #     return order_list, prob_list

    # @gen.coroutine
    # def get_alliance_orders(self, leader: str, allies:list = [], constraints:list = []) -> Dict[str, list]:
    #     """
    #     Return the orders that an alliance would take by merging the powers into one, then getting the orders of the merged power.
    #     :param leader: String representation of the power. The power that all other powers in the alliance will be merged into (temporarily)
    #     :param allies: List of string representations of all powers in the alliance that are NOT the leader.
    #     :returns: tuple of size 2: a list of sets of orders (sorted by power), along with a list of probabilities that correspond to the aforementioned list
    #         (
    #             [
    #                 {   # order_set1, corresponds with prob1
    #                     power_a: [order_x, order_y, ...],
    #                     power_b: [order_j, order_k, ...],
    #                     ...
    #                 },
    #                 {   # order_set2, corresponds with prob2
    #                     power_u: [order_a, order_b, ...],
    #                     power_v: [order_n, order_m, ...],
    #                     ...
    #                 },
    #                 ...
    #             ],
    #             [
    #                 prob1,  # corresponds with order_set1
    #                 prob2,  # corresponds with order_set2
    #                 ...
    #             ]
    #         )
    #     """
        
    #     # Merging the powers together should handle not attacking friendly powers 
    #     merged_scenario, ally_locs = self._merge_forces(deepcopy(self.game), leader, allies)
        
    #     ally_locs.extend(constraints)

    #     with merged_scenario.current_state():
    #         # Handle case where AI does nothing, as merged alliances already yield victory by extending victory conditions in this case
    #         merged_scenario.victory = [34] * 7  
    #         merged_orders = yield self.player.get_beam_orders(merged_scenario, leader)

    #     order_sets = merged_orders[0]
    #     probability_sets = merged_orders[1]

    #     # Organize orders via power
    #     # Possible orders, use to filter
    #     possible_orders = {ally : mila_utils.get_possible_orders_for_power(self.game, ally) for ally in allies}
    #     possible_orders[leader] = mila_utils.get_possible_orders_for_power(self.game, leader)

    #     alliance_orders = []

    #     for order_set in order_sets:
    #         sorted_orders = defaultdict(list)
    #         for order in order_set:
    #             if order in possible_orders[leader] and not self._does_attack_constraint(order, ally_locs):
    #                 sorted_orders[leader].append(order)
    #             else:
    #                 for ally in allies:
    #                     if order in possible_orders[ally] and not self._does_attack_constraint(order, ally_locs):
    #                         sorted_orders[ally].append(order)
    #                         break
    #         alliance_orders.append(dict(sorted_orders))

    #     return alliance_orders, probability_sets

    # def _get_targets_from_orders(self, game: Game, strategy_list: list) -> List[Dict[str, Set]]:
    #     """
    #     Helper function that extracts the powers targeted by a set of orders. Made to be used in conjunction with get_alliance_orders().
    #     :param game: MILA game object - encapsulates state and everything needed to play the game
    #     :param strategy_list: A list of dictionaries, each dictionary with key:value of power:order_list.
    #     :return: list of dictionaries, with format:
    #         [
    #             {
    #                 power1: [target_power_a, target_power_b,...],
    #                 power2: [target_power_a, target_power_c,...]
    #                 ...
    #             },
    #             {
    #                 power1: [target_power_c, target_power_z,...],
    #                 power2: [target_power_a, target_power_c,...]
    #                 ...
    #             }
    #         ]
    #     """
    #     target_list = []

    #     with game.current_state():
    #         for strategy in strategy_list:
    #             targets = {}
    #             for power, orders in strategy.items():
    #                 targets[power] = mila_utils.get_attacked_powers(game, orders)
    #             target_list.append(targets)

    #     return target_list