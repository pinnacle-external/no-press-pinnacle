import pytest

from diplomacy import Game
from utils.graph import Graph, Node, Front
from utils.utils import create_logger
from utils.constants import *

logger = create_logger("test_graphs")

ALLIANCE_1 = [(ENGLAND, FRANCE, GERMANY), (TURKEY, ITALY)]
ALLIANCE_2 = [(RUSSIA, GERMANY), (ENGLAND, ITALY)]


@pytest.fixture(scope='module')
def graph_init():
    game = Game()
    return Graph(game)

@pytest.fixture
def power_names():
    game = Game()
    return list(game.powers.keys())

class TestGraph:
    def test_init(self, graph_init):
        assert len(graph_init) > 0

    def test_get_owned_locs(self, graph_init):
        russia_owned = graph_init.get_owned_locs('RUSSIA')
        moscow = graph_init.get_node('MOS')
        logger.info([str(loc) for loc in russia_owned])
        assert type(russia_owned) == list
        assert len(russia_owned) > 0
        assert moscow in russia_owned

    @pytest.mark.parametrize("alliances,count", [(ALLIANCE_1, 0), (ALLIANCE_2, 0)])
    def test_get_fronts(self, graph_init, power_names, alliances, count):
        fronts = graph_init.get_fronts(power_names, alliances)
        for power_name, front_list in fronts.items():
            logger.info("{} Fronts: {}".format(power_name, [str(front) for front in front_list]))

        assert len(fronts) == count # Fronts should be empty, as this is the beginning of the game

class TestNode:
    def test_init(self, graph_init):
        node = graph_init.get_node('UKR')
        logger.info(str(node))
        assert type(node) == Node

    def test_neighbors(self, graph_init):
        node = graph_init.get_node('UKR')

        assert len(node.land_neighbors) > 0   # Ukraine should have multiple land neighbors
        assert len(node.water_neighbors) == 0 # Ukraine should be landlocked
        assert len(node.neighbors) > 0    # Combined, the neighbors should be greater than zero
        assert not node.is_neighbor('ENG')  # England should not be a neighbor to Ukraine

    def test_has_coast(self, graph_init):
        node = graph_init.get_node('ENG')
        
        assert node.has_coast()  # England should be adjacent to a water space

class TestFront:
    def test_init(self, graph_init):
        front = Front(set(graph_init.get_owned_locs('GERMANY')))
        logger.info(str(front))
        assert len(front) > 0

    def test_add(self, graph_init):
        front = Front(set(graph_init.get_owned_locs('GERMANY')))
        old_len = len(front)
        front.add(graph_init.get_node('BUD'))   # Budapest
        assert len(front) == old_len + 1

    def test_adjacents(self, graph_init):
        front = Front(set(graph_init.get_owned_locs('GERMANY')))
        
        adj = front.get_adjacents()   # Budapest
        logger.info([str(loc) for loc in adj])
        assert len(adj) > 0

