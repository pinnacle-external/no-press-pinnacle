from itertools import permutations
import pytest

from diplomacy import Game

from no_press_pinnacle.messages.orders import *
from no_press_pinnacle.messages.message_utils import shade_to_daide_unit
from no_press_pinnacle.utils.utils import create_logger
from no_press_pinnacle.utils.constants import *

logger = create_logger("test_orders")

POWER_GRP_1 = [ENG, FRA, ITA]
POWER_GRP_2 = [GER, RUS]
POWER_GRP_3 = [RUS]
POWER_GRP_4 = [AUS, TUR]
POWER_GRP_5 = [AUS, ENG, FRA, GER, ITA, RUS, TUR]

LOC_GRP_1 = [Location("BEL"), Location("UKR")]
LOC_GRP_2 = [Location("BLA"), Location("SEV"), Location("STP", "SCS")]
LOC_GRP_3 = [Location("BAL"), Location("BEL"), Location("BOH"), Location("GAS"), Location("GRE"), Location("VEN"), Location("WAR")]

SHADE_UNIT_LIST = []
DAIDE_UNIT_LIST = []

a_game = Game()
with a_game.current_state():
    for power_name, power in a_game.powers.items():
        SHADE_UNIT_LIST.append(power.units[0])
        DAIDE_UNIT_LIST.append(shade_to_daide_unit(power.units[0], a_game, power_name))

@pytest.fixture
def standard_order_1():
    unit1 = ArmyUnit("RUSSIA", "MOS")
    return Move(unit1, "SEV")

@pytest.fixture
def standard_order_2():
    unit = ArmyUnit("R")
    dest = "ANK"
    path = [Location("SEV"), Location("BLA"), Location("ANK")]
    return MoveByConvoy(unit, dest, path)


class TestDAIDEStringToOrder:

    @pytest.mark.parametrize("unit", DAIDE_UNIT_LIST)
    def test_daide_s2o_hold(self, unit):
        string = Hold.from_daide_str("({}) HLD".format(unit))
        obj = Hold(Unit.from_daide_str(unit))

        assert string == obj

    @pytest.mark.parametrize("unit, loc", zip(DAIDE_UNIT_LIST, LOC_GRP_3))
    def test_daide_s2o_move(self, unit, loc):
        string = Move.from_daide_str("({}) MTO {}".format(unit, loc))
        obj = Move(Unit.from_daide_str(unit), loc)

        assert string == obj

    @pytest.mark.parametrize("unit, target", permutations(DAIDE_UNIT_LIST, 2))
    def test_daide_s2o_support_hold(self, unit, target):
        string = SupportHold.from_daide_str("({}) SUP ({})".format(unit, target))
        obj = SupportHold(Unit.from_daide_str(unit), Unit.from_daide_str(target))

        assert string == obj

    @pytest.mark.parametrize("unit, target, dest", [("RUS AMY SEV", "RUS AMY UKR", "GAL"), ("FRA FLT (SPA NC)", "FRA FLT GOL", "PIE")])
    def test_daide_s2o_support_move(self, unit, target, dest):
        raw_str = "({}) SUP ({}) MTO {}".format(unit, target, dest)
        string = SupportMove.from_daide_str(raw_str)
        obj = SupportMove(Unit.from_daide_str(unit), Unit.from_daide_str(target), Location.from_daide_str(dest))

        assert string == obj

    @pytest.mark.parametrize("unit, ship, dest", [("RUS AMY SEV", "RUS FLT BLA", "ANK"), ("FRA AMY (SPA NC)", "FRA FLT GOL", "PIE")])
    def test_daide_s2o_convoy(self, unit, ship, dest):
        string = Convoy.from_daide_str("({}) CVY ({}) CTO {}".format(ship, unit, dest))
        obj = Convoy(Unit.from_daide_str(unit), Unit.from_daide_str(ship), Location.from_daide_str(dest))

        assert string == obj

    @pytest.mark.parametrize("unit, dest, path", [("RUS AMY SEV", "ANK", ["BLA"]), ("FRA AMY (SPA NC)", "PIE", ["GOL"])])
    def test_daide_s2o_move_by_convoy(self, unit, dest, path):
        string = MoveByConvoy.from_daide_str("({}) CTO {} VIA ({})".format(unit, dest, " ".join(path)))
        obj = MoveByConvoy(Unit.from_daide_str(unit), Location.from_daide_str(dest), [Location(l) for l in path])

        assert string == obj

    @pytest.mark.parametrize("unit, loc", zip(DAIDE_UNIT_LIST, LOC_GRP_3))
    def test_daide_s2o_retreat(self, unit, loc):
        string = Retreat.from_daide_str("({}) RTO {}".format(unit, loc))
        obj = Retreat(Unit.from_daide_str(unit), loc)

        assert string == obj

    @pytest.mark.parametrize("unit", DAIDE_UNIT_LIST)
    def test_daide_s2o_build(self, unit):
        string = Build.from_daide_str("({}) BLD".format(unit))
        obj = Build(Unit.from_daide_str(unit))

        assert string == obj

    @pytest.mark.parametrize("unit", DAIDE_UNIT_LIST)
    def test_daide_s2o_remove(self, unit):
        string = Remove.from_daide_str("({}) REM".format(unit))
        obj = Remove(Unit.from_daide_str(unit))

        assert string == obj

    @pytest.mark.parametrize("power", POWER_GRP_1)
    def test_daide_s2o_waive(self, power):
        string = WaiveBuild.from_daide_str("{} WVE".format(power))
        obj = WaiveBuild(power)

        assert string == obj


class TestSHADEStringToOrder:

    @pytest.mark.parametrize("unit, power", zip(SHADE_UNIT_LIST, POWER_GRP_5))
    def test_shade_s2o_hold(self, unit, power):
        string = Hold.from_shade_str("{} H".format(unit), power)
        obj = Hold(Unit.from_shade_str(unit, power))

        assert string == obj

    @pytest.mark.parametrize("unit, loc, power", zip(SHADE_UNIT_LIST, LOC_GRP_3, POWER_GRP_5))
    def test_shade_s2o_move(self, unit, loc, power):
        string = Move.from_shade_str("{} - {}".format(unit, loc.to_shade_str()), power)
        obj = Move(Unit.from_shade_str(unit, power), loc)

        assert string == obj

    @pytest.mark.parametrize("unit, target", permutations(SHADE_UNIT_LIST, 2))
    def test_shade_s2o_support_hold(self, unit, target):
        # NOTE: These units are being initiated without associated powers
        string = SupportHold.from_shade_str("{} S {}".format(unit, target))
        obj = SupportHold(Unit.from_shade_str(unit), Unit.from_shade_str(target))

        assert string == obj

    @pytest.mark.parametrize("unit, target, dest", [("A SEV", "A UKR", "GAL"), ("F SPA/NC", "F GOL", "PIE")])
    def test_shade_s2o_support_move(self, unit, target, dest):
        # NOTE: These units are being initiated without associated powers
        string = SupportMove.from_shade_str("{} S {} - {}".format(unit, target, dest))
        obj = SupportMove(Unit.from_shade_str(unit), Unit.from_shade_str(target), Location.from_shade_str(dest))

        assert string == obj

    @pytest.mark.parametrize("unit, ship, dest", [("A SEV", "F BLA", "ANK"), ("A SPA/NC", "F GOL", "PIE")])
    def test_shade_s2o_convoy(self, unit, ship, dest):
        # NOTE: These units are being initiated without associated powers
        string = Convoy.from_shade_str("{} C {} - {}".format(ship, unit, dest))
        obj = Convoy(Unit.from_shade_str(unit), Unit.from_shade_str(ship), Location.from_shade_str(dest))

        assert string == obj

    @pytest.mark.parametrize("unit, dest", [("A SEV", "ANK"), ("A SPA/NC", "PIE")])
    def test_shade_s2o_move_by_convoy(self, unit, dest):
        # NOTE: These units are being initiated without associated powers
        string = MoveByConvoy.from_shade_str("{} - {} VIA".format(unit, dest))
        obj = MoveByConvoy(Unit.from_shade_str(unit), Location.from_shade_str(dest), [])  # NOTE: This test will be conducted without a path

        assert string == obj

    @pytest.mark.parametrize("unit, loc, power", zip(SHADE_UNIT_LIST, LOC_GRP_3, POWER_GRP_5))
    def test_shade_s2o_retreat(self, unit, loc, power):
        string = Retreat.from_shade_str("{} R {}".format(unit, loc.to_shade_str()), power)
        obj = Retreat(Unit.from_shade_str(unit, power), loc)

        assert string == obj

    @pytest.mark.parametrize("unit, power", zip(SHADE_UNIT_LIST, POWER_GRP_5))
    def test_shade_s2o_build(self, unit, power):
        string = Build.from_shade_str("{} B".format(unit), power)
        obj = Build(Unit.from_shade_str(unit, power))

        assert string == obj

    @pytest.mark.parametrize("unit, power", zip(SHADE_UNIT_LIST, POWER_GRP_5))
    def test_shade_s2o_remove(self, unit, power):
        string = Remove.from_shade_str("{} D".format(unit), power)
        obj = Remove(Unit.from_shade_str(unit, power))

        assert string == obj

    @pytest.mark.parametrize("power", POWER_GRP_1)
    def test_shade_s2o_waive(self, power):
        string = WaiveBuild.from_shade_str("WAIVE")
        obj = WaiveBuild('000') # NOTE: No way to tell power from shade string

        assert string == obj


class TestOrderToSHADEString:

    @pytest.mark.parametrize("unit, power", zip(SHADE_UNIT_LIST, POWER_GRP_5))
    def test_shade_o2s_hold(self, unit, power):
        string = "{} H".format(unit)
        obj = Hold(Unit.from_shade_str(unit, power)).to_shade_str()

        assert string == obj

    @pytest.mark.parametrize("unit, loc, power", zip(SHADE_UNIT_LIST, LOC_GRP_3, POWER_GRP_5))
    def test_shade_o2s_move(self, unit, loc, power):
        string = "{} - {}".format(unit, loc.to_shade_str())
        obj = Move(Unit.from_shade_str(unit, power), loc).to_shade_str()

        assert string == obj

    @pytest.mark.parametrize("unit, target", permutations(SHADE_UNIT_LIST, 2))
    def test_shade_o2s_support_hold(self, unit, target):
        # NOTE: These units are being initiated without associated powers
        string = "{} S {}".format(unit, target)
        obj = SupportHold(Unit.from_shade_str(unit), Unit.from_shade_str(target)).to_shade_str()

        assert string == obj

    @pytest.mark.parametrize("unit, target, dest", [("A SEV", "A UKR", "GAL"), ("F SPA/NC", "F GOL", "PIE")])
    def test_shade_o2s_support_move(self, unit, target, dest):
        # NOTE: These units are being initiated without associated powers
        string = "{} S {} - {}".format(unit, target, dest)
        obj = SupportMove(Unit.from_shade_str(unit), Unit.from_shade_str(target), Location.from_shade_str(dest)).to_shade_str()

        assert string == obj

    @pytest.mark.parametrize("unit, ship, dest", [("A SEV", "F BLA", "ANK"), ("A SPA/NC", "F GOL", "PIE")])
    def test_shade_o2s_convoy(self, unit, ship, dest):
        # NOTE: These units are being initiated without associated powers
        string = "{} C {} - {}".format(ship, unit, dest)
        obj = Convoy(Unit.from_shade_str(unit), Unit.from_shade_str(ship), Location.from_shade_str(dest)).to_shade_str()

        assert string == obj

    @pytest.mark.parametrize("unit, dest", [("A SEV", "ANK"), ("A SPA/NC", "PIE")])
    def test_shade_o2s_move_by_convoy(self, unit, dest):
        # NOTE: These units are being initiated without associated powers
        string = "{} - {} VIA".format(unit, dest)

        # NOTE: This test will be conducted without a path
        obj = MoveByConvoy(Unit.from_shade_str(unit), Location.from_shade_str(dest), []).to_shade_str()

        assert string == obj

    @pytest.mark.parametrize("unit, loc, power", zip(SHADE_UNIT_LIST, LOC_GRP_3, POWER_GRP_5))
    def test_shade_o2s_retreat(self, unit, loc, power):
        string = "{} R {}".format(unit, loc.to_shade_str())
        obj = Retreat(Unit.from_shade_str(unit, power), loc).to_shade_str()

        assert string == obj

    @pytest.mark.parametrize("unit, power", zip(SHADE_UNIT_LIST, POWER_GRP_5))
    def test_shade_o2s_build(self, unit, power):
        string = "{} B".format(unit)
        obj = Build(Unit.from_shade_str(unit, power)).to_shade_str()

        assert string == obj

    @pytest.mark.parametrize("unit, power", zip(SHADE_UNIT_LIST, POWER_GRP_5))
    def test_shade_o2s_remove(self, unit, power):
        string = "{} D".format(unit)
        obj = Remove(Unit.from_shade_str(unit, power)).to_shade_str()

        assert string == obj

    @pytest.mark.parametrize("power", POWER_GRP_1)
    def test_shade_o2s_waive(self, power):
        string = "WAIVE"
        obj = WaiveBuild(power).to_shade_str()

        assert string == obj

class TestOrderToDAIDEString:

    @pytest.mark.parametrize("unit", DAIDE_UNIT_LIST)
    def test_daide_o2s_hold(self, unit):
        string = "({}) HLD".format(unit)
        obj = str(Hold(Unit.from_daide_str(unit)))

        assert string == obj

    @pytest.mark.parametrize("unit, loc", zip(DAIDE_UNIT_LIST, LOC_GRP_3))
    def test_daide_o2s_move(self, unit, loc):
        string = "({}) MTO {}".format(unit, loc)
        obj = str(Move(Unit.from_daide_str(unit), loc))

        assert string == obj

    @pytest.mark.parametrize("unit, target", permutations(DAIDE_UNIT_LIST, 2))
    def test_daide_o2s_support_hold(self, unit, target):
        string = "({}) SUP ({})".format(unit, target)
        obj = str(SupportHold(Unit.from_daide_str(unit), Unit.from_daide_str(target)))

        assert string == obj

    @pytest.mark.parametrize("unit, target, dest", [("RUS AMY SEV", "RUS AMY UKR", "GAL"), ("FRA FLT (SPA NC)", "FRA FLT GOL", "PIE")])
    def test_daide_o2s_support_move(self, unit, target, dest):
        string = "({}) SUP ({}) MTO {}".format(unit, target, dest)
        obj = str(SupportMove(Unit.from_daide_str(unit), Unit.from_daide_str(target), Location.from_daide_str(dest)))

        assert string == obj

    @pytest.mark.parametrize("unit, ship, dest", [("RUS AMY SEV", "RUS FLT BLA", "ANK"), ("FRA AMY (SPA NC)", "FRA FLT GOL", "PIE")])
    def test_daide_o2s_convoy(self, unit, ship, dest):
        string = "({}) CVY ({}) CTO {}".format(ship, unit, dest)
        obj = str(Convoy(Unit.from_daide_str(unit), Unit.from_daide_str(ship), Location.from_daide_str(dest)))

        assert string == obj

    @pytest.mark.parametrize("unit, dest, path", [("RUS AMY SEV", "ANK", ["BLA"]), ("FRA AMY (SPA NC)", "PIE", ["GOL"])])
    def test_daide_o2s_move_by_convoy(self, unit, dest, path):
        string = "({}) CTO {} VIA ({})".format(unit, dest, " ".join(path))
        obj = str(MoveByConvoy(Unit.from_daide_str(unit), Location.from_daide_str(dest), path))

        assert string == obj

    @pytest.mark.parametrize("unit, loc", zip(DAIDE_UNIT_LIST, LOC_GRP_3))
    def test_daide_o2s_retreat(self, unit, loc):
        string = "({}) RTO {}".format(unit, loc)
        obj = str(Retreat(Unit.from_daide_str(unit), loc))

        assert string == obj

    @pytest.mark.parametrize("unit", DAIDE_UNIT_LIST)
    def test_daide_o2s_build(self, unit):
        string = "({}) BLD".format(unit)
        obj = str(Build(Unit.from_daide_str(unit)))

        assert string == obj

    @pytest.mark.parametrize("unit", DAIDE_UNIT_LIST)
    def test_daide_o2s_remove(self, unit):
        string = "({}) REM".format(unit)
        obj = str(Remove(Unit.from_daide_str(unit)))

        assert string == obj

    @pytest.mark.parametrize("power", POWER_GRP_1)
    def test_daide_o2s_waive(self, power):
        string = "{} WVE".format(power)
        obj = str(WaiveBuild(power))

        assert string == obj