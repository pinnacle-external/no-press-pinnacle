import pytest

from diplomacy import Game

from no_press_pinnacle.messages.messages import *
from no_press_pinnacle.messages.message_utils import shade_to_daide_unit
from utils.mila_engine_utils import get_possible_orders_for_power
from utils.utils import create_logger, short2long
from utils.constants import *

logger = create_logger("test_messages")

POWER_GRP_1 = [ENG, FRA, ITA]
POWER_GRP_2 = [GER, RUS]
POWER_GRP_3 = [RUS]
POWER_GRP_4 = [AUS, TUR]
POWER_GRP_5 = [AUS, ENG, FRA, GER, ITA, RUS, TUR]

LOC_GRP_1 = [Location("BEL"), Location("UKR")]
LOC_GRP_2 = [Location("BLA"), Location("SEV"), Location("STP", "SCS")]
LOC_GRP_3 = [Location("BAL"), Location("BEL"), Location("BOH"), Location("GAS"), Location("GRE"), Location("VEN"), Location("WAR")]

@pytest.fixture
def standard_arrangement_1():
    return Ally(POWER_GRP_1, POWER_GRP_2)

@pytest.fixture
def standard_arrangement_2():
    return DMZ(POWER_GRP_1, LOC_GRP_1)


class TestMessageToString:
    def test_m2s_draw(self):
        assert Draw().compose() == "PRP(DRW)"

    @pytest.mark.parametrize("allies,enemies", [(POWER_GRP_1, POWER_GRP_3), (POWER_GRP_4, POWER_GRP_1)])
    def test_m2s_ally(self, allies, enemies):
        logger.info("Testing message to string ALY using: {} and {}".format(allies, enemies))
        assert Ally(allies, enemies).compose() == "PRP(ALY({})VSS({}))".format(" ".join(allies), " ".join(enemies))

    @pytest.mark.parametrize("powers", [POWER_GRP_1, POWER_GRP_4])
    def test_m2s_peace(self, powers):
        logger.info("Testing message to string PCE using: {}".format(powers))
        assert Peace(powers).compose() == "PRP(PCE({}))".format(" ".join(powers))

    @pytest.mark.parametrize("power", POWER_GRP_1)
    def test_m2s_solo(self, power):
        logger.info("Testing message to string SLO using: {}".format(power))
        assert Solo(power).compose() == "PRP(SLO({}))".format(power)

    @pytest.mark.parametrize("power", POWER_GRP_1)
    def test_m2s_order(self, power):
        game = Game()
        logger.info("Testing message to string XDO using: {}".format(power))

        orders = get_possible_orders_for_power(game, short2long(power))

        for order in orders:
            o = MsgOrder(Order.from_shade_str(order, power=power)).compose()
            assert o == "PRP(XDO({}))".format(Order.from_shade_str(order, power=power))

    @pytest.mark.parametrize("powers,locs", [(POWER_GRP_1, LOC_GRP_1), (POWER_GRP_2, LOC_GRP_2)])
    def test_m2s_dmz(self, powers, locs):
        logger.info("Testing message to string DMZ using: {} and {}".format(powers, locs))
        locations = [str(l) for l in locs]
        assert DMZ(powers, locs).compose() == "PRP(DMZ({})({}))".format(" ".join(powers), " ".join(locations))

    def test_m2s_yes(self, standard_arrangement_1):
        assert Yes(standard_arrangement_1).compose() == "YES({})".format(standard_arrangement_1)

    def test_m2s_reject(self, standard_arrangement_1):
        assert Reject(standard_arrangement_1).compose() == "REJ({})".format(standard_arrangement_1)

    def test_m2s_bsx(self, standard_arrangement_1):
        assert Beeswax(standard_arrangement_1).compose() == "BSX({})".format(standard_arrangement_1)

    def test_m2s_cancel(self, standard_arrangement_1):
        assert Cancel(standard_arrangement_1).compose() == "CCL({})".format(standard_arrangement_1)

    def test_m2s_statement(self, standard_arrangement_1):
        assert Statement(standard_arrangement_1).compose() == "FCT({})".format(standard_arrangement_1)

    def test_m2s_not(self, standard_arrangement_1):
        assert Not(standard_arrangement_1).compose() == "PRP(NOT({}))".format(standard_arrangement_1)

    def test_m2s_and(self, standard_arrangement_1, standard_arrangement_2):
        arr_string = And([standard_arrangement_1, standard_arrangement_2]).compose()

        assert arr_string == "PRP(AND({})({}))".format(standard_arrangement_1, standard_arrangement_2)

    def test_m2s_orr(self, standard_arrangement_1, standard_arrangement_2):
        arr_string = Orr([standard_arrangement_1, standard_arrangement_2]).compose()

        assert arr_string == "PRP(ORR({})({}))".format(standard_arrangement_1, standard_arrangement_2)


class TestStringToMessage:
    def test_s2m_draw(self):
        assert Message.from_daide_str("PRP(DRW)") == Draw()

    @pytest.mark.parametrize("allies, enemies", [(POWER_GRP_1, POWER_GRP_3), (POWER_GRP_4, POWER_GRP_2)])
    def test_s2m_ally(self, allies, enemies):
        ally_list = " ".join(allies)
        enemy_list = " ".join(enemies)
        from_str = Message.from_daide_str("PRP(ALY({})VSS({}))".format(ally_list, enemy_list))
        assert from_str == Ally(allies, enemies)

    @pytest.mark.parametrize("powers", [POWER_GRP_1, POWER_GRP_4])
    def test_s2m_peace(self, powers):
        power_list = " ".join(powers)
        from_str = Message.from_daide_str("PRP(PCE({}))".format(power_list))
        assert from_str == Peace(powers)

    @pytest.mark.parametrize("power", POWER_GRP_1)
    def test_s2m_solo(self, power):
        assert Message.from_daide_str("PRP(SLO({}))".format(power)) == Solo(power)

    @pytest.mark.parametrize("power", POWER_GRP_1)
    def test_s2m_order(self, power):
        game = Game()
        
        orders = get_possible_orders_for_power(game, short2long(power))

        for order in orders:
            daide_order = Order.from_shade_str(order)
            from_str = Message.from_daide_str("PRP(XDO({}))".format(str(daide_order)))
            assert from_str == MsgOrder(daide_order) 

    @pytest.mark.parametrize("powers,locs", [(POWER_GRP_1, LOC_GRP_1), (POWER_GRP_4, LOC_GRP_2)])
    def test_s2m_dmz(self, powers, locs):
        locations = [str(l) for l in locs]
        from_str = Message.from_daide_str("PRP(DMZ({})({}))".format(" ".join(powers), " ".join(locations)))
        obj = DMZ(powers, locs)

        assert from_str == obj

    def test_s2m_yes(self, standard_arrangement_1):
        from_str = Message.from_daide_str("YES({})".format(standard_arrangement_1))

        assert from_str == Yes(standard_arrangement_1)

    def test_s2m_reject(self, standard_arrangement_1):
        from_str = Message.from_daide_str("REJ({})".format(standard_arrangement_1))

        assert from_str == Reject(standard_arrangement_1)

    def test_s2m_bsx(self, standard_arrangement_1):
        from_str = Message.from_daide_str("BSX({})".format(standard_arrangement_1))

        assert from_str == Beeswax(standard_arrangement_1)

    def test_s2m_cancel(self, standard_arrangement_1):
        from_str = Message.from_daide_str("CCL({})".format(standard_arrangement_1))

        assert from_str == Cancel(standard_arrangement_1)

    def test_s2m_statement(self, standard_arrangement_1):
        from_str = Message.from_daide_str("FCT({})".format(standard_arrangement_1))

        assert from_str == Statement(standard_arrangement_1)

    def test_s2m_not(self, standard_arrangement_1):
        from_str = Message.from_daide_str("PRP(NOT({}))".format(standard_arrangement_1))

        assert from_str == Not(standard_arrangement_1)

    def test_s2m_and(self, standard_arrangement_1, standard_arrangement_2):
        from_str = Message.from_daide_str("PRP(AND({})({}))".format(standard_arrangement_1, standard_arrangement_2))

        assert from_str == And([standard_arrangement_1, standard_arrangement_2])

    def test_s2m_orr(self, standard_arrangement_1, standard_arrangement_2):
        from_str = Message.from_daide_str("PRP(ORR({})({}))".format(standard_arrangement_1, standard_arrangement_2))

        assert from_str == Orr([standard_arrangement_1, standard_arrangement_2])